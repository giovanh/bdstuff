//META{"name":"MessageLoggerV2","source":"https://gitlab.com/_Lighty_/bdstuff/blob/master/MessageLoggerV2.plugin.js","website":"https://_lighty_.gitlab.io/bdstuff/"}*//
/*@cc_on
@if (@_jscript)
    // Offer to self-install for clueless users that try to run this directly.
    var shell = WScript.CreateObject("WScript.Shell");
    var fs = new ActiveXObject("Scripting.FileSystemObject");
    var pathPlugins = shell.ExpandEnvironmentStrings("%APPDATA%\\BetterDiscord\\plugins");
    var pathSelf = WScript.ScriptFullName;
    // Put the user at ease by addressing them in the first person
    shell.Popup("It looks like you mistakenly tried to run me directly. (don't do that!)", 0, "I'm a plugin for BetterDiscord", 0x30);
    if (fs.GetParentFolderName(pathSelf) === fs.GetAbsolutePathName(pathPlugins)) {
        shell.Popup("I'm in the correct folder already.\nJust reload Discord with Ctrl+R.", 0, "I'm already installed", 0x40);
    } else if (!fs.FolderExists(pathPlugins)) {
        shell.Popup("I can't find the BetterDiscord plugins folder.\nAre you sure it's even installed?", 0, "Can't install myself", 0x10);
    } else if (shell.Popup("Should I copy myself to BetterDiscord's plugins folder for you?", 0, "Do you need some help?", 0x34) === 6) {
        fs.CopyFile(pathSelf, fs.BuildPath(pathPlugins, fs.GetFileName(pathSelf)), true);
        // Show the user where to put plugins in the future
        shell.Exec("explorer " + pathPlugins);
        shell.Popup("I'm installed!\nJust reload Discord with Ctrl+R.", 0, "Successfully installed", 0x40);
    }
    WScript.Quit();
@else @*/
// extra TODOs:
// special edited message https://i.clouds.tf/guli/mric.png
// modal for checking which servers/channels/users are blacklisted/whitelisted
// option to show all hidden
// menu is unusable in light theme
class MessageLoggerV2 {
    getName() {
        return 'MessageLoggerV2';
    }
    getVersion() {
        return '1.5.3';
    }
    getAuthor() {
        return 'Lighty';
    }
    getDescription() {
        return "Recode of Metalloriff's MessageLogger plugin. Records all sent messages, message edits and message deletions in the specified servers, all unmuted servers or all servers, and in direct messages.";
    }
    load() {}
    start() {
        let onLoaded = () => {
            try {
                if (!global.ZeresPluginLibrary || !(this.localUser = ZLibrary.DiscordModules.UserStore.getCurrentUser())) setTimeout(() => onLoaded(), 1000);
                else this.initialize();
            } catch (err) {
                ZLibrary.Logger.stacktrace(this.getName(), 'Failed to start!', err);
                ZLibrary.Logger.err(this.getName(), `If you cannot solve this yourself, contact ${this.getAuthor()} and provide the errors shown here.`);
                this.stop();
                this.showToast(`[${this.getName()}] Failed to start! Check console (CTRL + SHIFT + I, click console tab) for more error info.`, { type: 'error', timeout: 10000 });
            }
        };
        const getDir = () => {
            // from Zeres Plugin Library, copied here as ZLib may not be available at this point
            const process = require('process');
            const path = require('path');
            if (process.env.injDir) return path.resolve(process.env.injDir, 'plugins/');
            switch (process.platform) {
                case 'win32':
                    return path.resolve(process.env.appdata, 'BetterDiscord/plugins/');
                case 'darwin':
                    return path.resolve(process.env.HOME, 'Library/Preferences/', 'BetterDiscord/plugins/');
                default:
                    return path.resolve(process.env.XDG_CONFIG_HOME ? process.env.XDG_CONFIG_HOME : process.env.HOME + '/.config', 'BetterDiscord/plugins/');
            }
        };
        this.pluginDir = getDir();
        let libraryOutdated = false;
        // I'm sick and tired of people telling me my plugin doesn't work and it's cause zlib is outdated, ffs
        if (!global.ZLibrary || !global.ZeresPluginLibrary || (bdplugins.ZeresPluginLibrary && (libraryOutdated = ZeresPluginLibrary.PluginUpdater.defaultComparator(bdplugins.ZeresPluginLibrary.plugin._config.info.version, '1.2.6')))) {
            const title = libraryOutdated ? 'Library outdated' : 'Library Missing';
            const ModalStack = BdApi.findModuleByProps('push', 'update', 'pop', 'popWithKey');
            const TextElement = BdApi.findModuleByProps('Sizes', 'Weights');
            const ConfirmationModal = BdApi.findModule(m => m.defaultProps && m.key && m.key() == 'confirm-modal');
            const confirmedDownload = () => {
                require('request').get('https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js', async (error, response, body) => {
                    if (error) return require('electron').shell.openExternal('https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js');
                    require('fs').writeFile(require('path').join(this.pluginDir, '0PluginLibrary.plugin.js'), body, () => {
                        setTimeout(() => {
                            if (!global.bdplugins.ZeresPluginLibrary) return BdApi.alert('Notice', `Due to you using EnhancedDiscord instead of BetterDiscord, you'll have to reload your Discord before ${this.getName()} starts working. Just press CTRL + R to reload and ${this.getName()} will begin to work!`);
                            onLoaded();
                        }, 1000);
                    });
                });
            };
            if (!ModalStack || !ConfirmationModal || !TextElement) {
                BdApi.alert('Uh oh', `Looks like you${libraryOutdated ? 'r Zeres Plugin Library was outdated!' : ' were missing Zeres Plugin Library!'} Also, failed to show a modal, so it has been ${libraryOutdated ? 'updated' : 'downloaded and loaded'} automatically.`);
                confirmedDownload();
                return;
            }
            ModalStack.push(props => {
                return BdApi.React.createElement(
                    ConfirmationModal,
                    Object.assign(
                        {
                            header: title,
                            children: [TextElement({ color: TextElement.Colors.PRIMARY, children: [`The library plugin needed for ${this.getName()} is ${libraryOutdated ? 'outdated' : 'missing'}. Please click Download Now to ${libraryOutdated ? 'update' : 'install'} it.`] })],
                            red: false,
                            confirmText: 'Download Now',
                            cancelText: 'Cancel',
                            onConfirm: () => confirmedDownload()
                        },
                        props
                    )
                );
            });
        } else onLoaded();
    }
    stop() {
        try {
            this.shutdown();
        } catch (err) {
            ZLibrary.Logger.stacktrace(this.getName(), 'Failed to stop!', err);
        }
    }
    getChanges() {
        return [
            {
                title: 'added',
                type: 'added',
                items: [
                    'Added help modal to help with hidden features and whitelist/blacklist/ignore system, button is in settings and menu help',
                    'Added option to disable toasts for deleted/edited messages from local user (you)',
                    'Added max shown edits option. Allows you to limit max number of shown edits in chat only to avoid spam.',
                    'Added experimental support for EnhancedDiscord',
                    'You can now share this plugin via my site! https://_lighty_.gitlab.io/bdstuff/'
                ]
            },
            {
                title: 'fixes',
                type: 'fixed',
                items: ['Fixed crash related to link embeds loading in after the message was edited. (thanks @Temm for helping me reproduce the bug!)', 'Whitelist ignores NSFW ignore setting', 'Fixed delete clicking a message in the menu not removing it from chat']
            },
            {
                title: 'Improved',
                type: 'improved',
                items: [
                    'Links in edited messages are darkened like normal text to differentiate between edit and current message',
                    'Edited message color is no longer hardcoded, it is now simply normal text color but darkened, to match theme colors.',
                    'Toasts no longer show for local user in DMs (you did it, why show it?).',
                    'Ghost ping toast no longer shows if you are in the channel.'
                ]
            },

            {
                title: 'Gimme some time',
                type: 'progress',
                items: ['Expand Stats modal to show per server and per channel stats, also show disk space usage.', 'Light theme support']
            }
        ];
    }
    initialize() {
        let defaultSettings = {
            developerMode: false,
            autoBackup: false,
            dontSaveData: false,
            displayUpdateNotes: true,
            ignoreMutedGuilds: true,
            ignoreMutedChannels: true,
            ignoreBots: true,
            ignoreSelf: false,
            ignoreBlockedUsers: true,
            ignoreNSFW: false,
            showOpenLogsButton: true,
            messageCacheCap: 1000,
            savedMessagesCap: 100,
            reverseOrder: true,
            onlyLogWhitelist: false,
            whitelist: [],
            blacklist: [],
            toastToggles: {
                sent: false,
                edited: true,
                deleted: true,
                ghostPings: true
            },
            toastTogglesDMs: {
                sent: false,
                edited: true,
                deleted: true,
                ghostPings: true,
                disableToastsForLocal: false
            },
            disableKeybind: false,
            cacheAllImages: true,
            aggresiveMessageCaching: true, // make it more aggresive bruuh
            openLogKeybind: [
                /* 162, 77 */
            ], // ctrl + m on windows
            openLogFilteredKeybind: [
                /* 162, 78 */
            ], // ctrl + n on windows
            renderCap: 50,
            maxShownEdits: 0,
            hideNewerEditsFirst: true,
            displayDates: true,
            deletedMessageColor: '',
            editedMessageColor: '',
            showEditedMessages: true,
            showDeletedMessages: true,
            showPurgedMessages: true,
            showDeletedCount: true,
            showEditedCount: true,
            alwaysLogSelected: false,
            alwaysLogDM: true,
            restoreDeletedMessages: true,
            versionInfo: ''
        };

        this.settings = ZLibrary.PluginUtilities.loadSettings(this.getName(), defaultSettings);
        let settingsChanged = false;

        if (!this.settings || !Object.keys(this.settings).length) {
            this.showToast(`[${this.getName()}] Settings file corrupted! All settings restored to default.`, { type: 'error', timeout: 5000 });
            this.settings = defaultSettings; // todo: does defaultSettings get changed?
            settingsChanged = true;
        }
        if (!this.settings.openLogKeybind.length) {
            this.settings.openLogKeybind = [162, 77];
            settingsChanged = true;
        }
        if (!this.settings.openLogFilteredKeybind.length) {
            this.settings.openLogFilteredKeybind = [162, 78];
            settingsChanged = true;
        }
        this.settings.developerMode = false;

        if (settingsChanged) this.saveSettings();

        // TODO: maybe make a button to show changelog?
        if (!this.settings.developerMode) ZLibrary.PluginUpdater.checkForUpdate(this.getName(), this.getVersion(), 'https://_lighty_.gitlab.io/bdstuff/plugins/MessageLoggerV2.plugin.js');
        if (this.settings.versionInfo !== this.getVersion() && this.settings.displayUpdateNotes) {
            ZLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges());
            this.settings.versionInfo = this.getVersion();
            this.saveSettings();
        }

        this.nodeModules = {
            electron: require('electron'),
            request: require('request'),
            fs: require('fs')
        };

        let defaultConstruct = () => {
            return Object.assign(
                {},
                {
                    messageRecord: {},
                    deletedMessageRecord: {},
                    editedMessageRecord: {},
                    purgedMessageRecord: {}
                }
            );
        };
        let data;
        if (this.settings.dontSaveData) {
            data = defaultConstruct();
        } else {
            data = this.loadData(this.getName() + 'Data', defaultConstruct());
            const isBad = map => !(map && map.messageRecord && map.editedMessageRecord && map.deletedMessageRecord && map.purgedMessageRecord && typeof map.messageRecord == 'object' && typeof map.editedMessageRecord == 'object' && typeof map.deletedMessageRecord == 'object' && typeof map.purgedMessageRecord == 'object');
            if (isBad(data)) {
                if (this.settings.autoBackup) {
                    this.showToast(`[${this.getName()}] Data was corrupted! Loading backup.`, { type: 'info', timeout: 5000 });
                    data = this.loadData(this.getName() + 'DataBackup', defaultConstruct());
                    if (isBad(data)) {
                        this.showToast(`[${this.getName()}] Backup was corrupted! All deleted/edited/purged messages have been erased.`, { type: 'error', timeout: 10000 });
                        data = defaultConstruct();
                    }
                } else {
                    this.showToast(`[${this.getName()}] Data was corrupted! Recommended to turn on auto backup in settings! All deleted/edited/purged messages have been erased.`, { type: 'error', timeout: 10000 });
                    data = defaultConstruct();
                }
            }
        }

        if (!this.settings.dontSaveData) {
            const records = data.messageRecord;
            // data structure changed a wee bit, compensate instead of deleting user data or worse, erroring out
            for (let a in records) {
                const record = records[a];
                if (record.deletedata) {
                    if (record.deletedata.deletetime) {
                        record.delete_data = {};
                        record.delete_data.time = record.deletedata.deletetime;
                        record.delete_data.rel_ids = record.deletedata.relativeids;
                    }
                    delete record.deletedata;
                }
                if (record.editHistory) {
                    record.edit_history = [];
                    for (let b in record.editHistory) {
                        record.edit_history.push({ content: record.editHistory[b].content, time: record.editHistory[b].editedAt });
                    }
                    delete record.editHistory;
                }
                this.fixEmbeds(record.message);
            }
        }

        this.cachedMessageRecord = [];
        this.messageRecord = data.messageRecord;
        this.deletedMessageRecord = data.deletedMessageRecord;
        this.editedMessageRecord = data.editedMessageRecord;
        this.purgedMessageRecord = data.purgedMessageRecord;

        defaultConstruct = () => {
            return Object.assign(
                {},
                {
                    imageCacheRecord: {}
                }
            );
        };

        this.imageCacheDir = this.pluginDir + '/MLV2_IMAGE_CACHE';

        if (this.settings.cacheAllImages && !this.nodeModules.fs.existsSync(this.imageCacheDir)) this.nodeModules.fs.mkdirSync(this.imageCacheDir);

        if (this.settings.dontSaveData || !this.settings.cacheAllImages) {
            data = defaultConstruct(); // unused but just in case my stupidity gets in the way
        } else {
            data = this.loadData('MLV2_IMAGE_CACHE/ImageCache', defaultConstruct());
            const isBad = map => !(map && map.imageCacheRecord && typeof map.imageCacheRecord == 'object');
            if (isBad(data)) {
                data = defaultConstruct();
            }
        }

        this.imageCacheRecord = data.imageCacheRecord;

        defaultConstruct = undefined;

        this.tools = {
            openUserContextMenu: null /* NeatoLib.Modules.get('openUserContextMenu').openUserContextMenu */, // TODO: move here
            getMessage: ZLibrary.DiscordModules.MessageStore.getMessage,
            fetchMessages: ZLibrary.DiscordModules.MessageActions.fetchMessages,
            transitionTo: null /* NeatoLib.Modules.get('transitionTo').transitionTo */,
            getChannel: ZLibrary.DiscordModules.ChannelStore.getChannel,
            copyToClipboard: this.nodeModules.electron.clipboard.writeText,
            getServer: ZLibrary.DiscordModules.GuildStore.getGuild,
            getUser: ZLibrary.DiscordModules.UserStore.getUser,
            parse: ZLibrary.WebpackModules.getByProps('parserFor', 'parse').parse,
            getUserAsync: ZLibrary.WebpackModules.getByProps('getUser', 'acceptAgreements').getUser,
            isBlocked: ZLibrary.WebpackModules.getByProps('isBlocked').isBlocked,
            createMomentObject: ZLibrary.WebpackModules.getByProps('createFromInputFallback'),
            isMentioned: ZLibrary.WebpackModules.getByProps('isMentioned').isMentioned
        };

        this.createButton.classes = {
            button: (function() {
                let buttonData = ZLibrary.WebpackModules.getByProps('button', 'colorBrand');
                return `${buttonData.button} ${buttonData.lookFilled} ${buttonData.colorBrand} ${buttonData.sizeSmall} ${buttonData.grow}`;
            })(),
            buttonContents: ZLibrary.WebpackModules.getByProps('button', 'colorBrand').contents
        };

        this.createMessageGroup.classes = {
            containerBounded: ZLibrary.DiscordClasses.Messages.containerCozyBounded.value,
            message: ZLibrary.DiscordClasses.Messages.messageCozy.value,
            messageSingle: ZLibrary.DiscordClasses.Messages.messageCozy.value.split(/ /g)[0],
            header: ZLibrary.DiscordClasses.Messages.headerCozy.value,
            avatar: ZLibrary.DiscordClasses.Messages.avatar.value,
            headerMeta: ZLibrary.DiscordClasses.Messages.headerCozyMeta.value,
            username: ZLibrary.DiscordClasses.Messages.username.value,
            timestamp: ZLibrary.DiscordClasses.Messages.timestampCozy.value,
            timestampSingle: ZLibrary.DiscordClasses.Messages.timestampCozy.value.split(/ /g)[0],
            content: ZLibrary.DiscordClasses.Messages.contentCozy.value,
            avatarSingle: ZLibrary.DiscordClasses.Messages.avatar.value.split(/ /g)[0],
            avatarImg: ZLibrary.WebpackModules.getByProps('avatar', 'cursorDefault', 'mask').avatar,
            avatarImgSingle: ZLibrary.WebpackModules.getByProps('avatar', 'cursorDefault', 'mask').avatar.split(/ /g)[0],
            botTag: ZLibrary.WebpackModules.getByProps('botTagRegular').botTagRegular + ' ' + ZLibrary.WebpackModules.getByProps('botTagCozy').botTagCozy,
            markupSingle: ZLibrary.WebpackModules.getByProps('markup').markup.split(/ /g)[0]
        };

        this.multiClasses = {
            defaultColor: ZLibrary.WebpackModules.getByProps('defaultColor').defaultColor,
            item: ZLibrary.WebpackModules.find(m => m.item && m.selected && m.topPill).item,
            tabBarItem: ZLibrary.DiscordClassModules.UserModal.tabBarItem,
            tabBarContainer: ZLibrary.DiscordClassModules.UserModal.tabBarContainer,
            tabBar: ZLibrary.DiscordClassModules.UserModal.tabBar,
            edited: ZLibrary.WebpackModules.getByProps('edited').edited,
            markup: ZLibrary.WebpackModules.getByProps('markup')['markup'],
            message: {
                cozy: {
                    containerBounded: ZLibrary.DiscordClasses.Messages.containerCozyBounded.value,
                    message: ZLibrary.DiscordClasses.Messages.messageCozy.value,
                    header: ZLibrary.DiscordClasses.Messages.headerCozy.value,
                    avatar: ZLibrary.DiscordClasses.Messages.avatar.value,
                    headerMeta: ZLibrary.DiscordClasses.Messages.headerCozyMeta.value,
                    username: ZLibrary.DiscordClasses.Messages.username.value,
                    timestamp: ZLibrary.DiscordClasses.Messages.timestampCozy.value,
                    content: ZLibrary.DiscordClasses.Messages.contentCozy.value
                }
            }
        };

        this.classes = {
            markup: ZLibrary.WebpackModules.getByProps('markup')['markup'].split(/ /g)[0],
            content: ZLibrary.WebpackModules.getByProps('contentCozy')['content'].split(/ /g)[0],
            hidden: ZLibrary.WebpackModules.getByProps('spoilerText', 'hidden').hidden.split(/ /g)[0],
            chat: ZLibrary.WebpackModules.getByProps('chat').chat.split(/ /g)[0],
            messages: ZLibrary.DiscordClasses.Messages.message.value.split(/ /g)[0],
            tabBar: ZLibrary.DiscordClassModules.UserModal.tabBar.split(/ /g)[0],
            isCompact: ZLibrary.WebpackModules.getByProps('isCompact').isCompact.split(/ /g)[0],
            containerBounded: ZLibrary.DiscordClasses.Messages.containerCozyBounded.value.split(/ /g)[0]
        };

        this.muteModule = ZLibrary.WebpackModules.find(m => m.isMuted);

        this.menu = {};
        this.menu.classes = {};
        this.menu.filter = '';
        this.menu.open = false;

        this.createTextBox.classes = {
            inputWrapper: ZLibrary.WebpackModules.getByProps('inputWrapper').inputWrapper,
            inputMultiInput: ZLibrary.WebpackModules.getByProps('input').input + ' ' + ZLibrary.WebpackModules.getByProps('multiInput').multiInput,
            multiInputFirst: ZLibrary.WebpackModules.getByProps('multiInputFirst').multiInputFirst,
            inputDefaultMultiInputField: ZLibrary.WebpackModules.getByProps('inputDefault').inputDefault + ' ' + ZLibrary.WebpackModules.getByProps('multiInputField').multiInputField,
            questionMark: ZLibrary.WebpackModules.getByProps('questionMark').questionMark,
            icon: ZLibrary.WebpackModules.getByProps('questionMark').icon,
            focused: ZLibrary.WebpackModules.getByProps('focused').focused.split(/ /g),
            questionMarkSingle: ZLibrary.WebpackModules.getByProps('questionMark').questionMark.split(/ /g)[0]
        };

        this.createHeader.classes = {
            itemTabBarItem: ZLibrary.DiscordClassModules.UserModal.tabBarItem + ' ' + ZLibrary.WebpackModules.find(m => m.item && m.selected && m.topPill).item,
            tabBarContainer: ZLibrary.DiscordClassModules.UserModal.tabBarContainer,
            tabBar: ZLibrary.DiscordClassModules.UserModal.tabBar,
            tabBarSingle: ZLibrary.DiscordClassModules.UserModal.tabBar.split(/ /g)[0]
        };

        this.createModal.imageModal = ZLibrary.WebpackModules.getByDisplayName('ImageModal');
        this.createModal.confirmationModal = ZLibrary.DiscordModules.ConfirmationModal;

        this.observer.chatClass = ZLibrary.WebpackModules.getByProps('chat').chat.split(/ /g)[0];

        this.localUser = ZLibrary.DiscordModules.UserStore.getCurrentUser();

        this.deletedChatMessagesCount = {};
        this.editedChatMessagesCount = {};

        this.channelMessages = ZLibrary.WebpackModules.find(m => m._channelMessages)._channelMessages;

        this.autoBackupSaveInterupts = 0;

        this.unpatches = [];

        this.unpatches.push(ZLibrary.Patcher.instead(this.getName(), ZLibrary.WebpackModules.find(m => m.dispatch), 'dispatch', (_, args, original) => this.onDispatchEvent(args, original)));
        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.DiscordModules.MessageActions, 'startEditMessage', (_, args, original) => {
                const channelId = args[0];
                const messageId = args[1];
                if (!this.editedMessageRecord[channelId] || this.editedMessageRecord[channelId].findIndex(m => m === messageId) == -1) return original(...args);
                let record = this.getSavedMessage(messageId);
                if (!record) return original(...args);
                args[2] = record.message.content;
                return original(...args);
            })
        );

        // todo: maybe do it in dispatch? or use 'instead'
        this.unpatches.push(
            ZLibrary.Patcher.after(this.getName(), ZLibrary.DiscordModules.MessageActions, 'endEditMessage', (_, args, __) => {
                if (!args.length) setTimeout(() => this.updateMessages(), 0); // settimeout so it can do it after it was restored
            })
        );

        this.style = {};

        this.style.deleted = this.settings.developerMode ? 'ml-deleted' : this.randomString();
        this.style.edited = this.settings.developerMode ? 'ml-edited' : this.randomString();

        this.invalidateAllChannelCache();
        this.selectedChannel = this.getSelectedTextChannel();
        if (this.selectedChannel) {
            this.cacheChannelMessages(this.selectedChannel.id);
            /* setTimeout(() =>  */ this.updateMessages(); /* , 20); */ // why is this in a settimeout? TODO find out
        }

        // todo: custom deleted message text color
        ZLibrary.PluginUtilities.addStyle(
            (this.style.css = this.settings.developerMode ? 'ML2-CSS' : this.randomString()),
            `
            html > head + body #app-mount * .bda-links [href*="MessageLogger"]:after{
                display: none !important;
            }
            .da-container .da-markup, .da-container .${this.classes.markup} {
                /* transition: color 0.3s; */
            }
            .${this.style.deleted} .${this.classes.markup}, .${this.style.deleted} .${this.classes.markup} .hljs{
                color: #f04747 !important;
            }
            .${this.classes.markup}.${this.style.edited} .${this.style.edited} {
				filter: brightness(70%);
            }

            .${this.style.deleted}:not(:hover) .da-content img, .${this.style.deleted}:not(:hover) .mention, .${this.style.deleted}:not(:hover) .reactions, .${this.style.deleted}:not(:hover) .da-content a {
				filter: grayscale(100%) !important;
			}

			.${this.style.deleted} .da-content img, .${this.style.deleted} .mention, .${this.style.deleted} .reactions, .${this.style.deleted} .da-content a {
				transition: filter 0.3s !important;
            }

            .ml2-tab {
                border-color: transparent;
                color: rgba(255, 255, 255, 0.4);
            }

            .ml2-tab-selected {
                border-color: rgb(255, 255, 255);
                color: rgb(255, 255, 255);
            }

            .ml2-help-text-indent {
                margin-left: 40px;
            }
        `
        );
        this.patchModal();

        const createKeybindListener = () => {
            this.keybindListener = new (ZLibrary.WebpackModules.getModule(m => typeof m === 'function' && m.toString().includes('.default.setOnInputEventCallback')))();
            this.keybindListener.on('change', e => {
                if (this.settings.disableKeybind) return; // todo: destroy if disableKeybind is set to true and don't make one if it was true from the start
                // this is the hackiest thing ever but it works xdd
                if (!ZLibrary.WebpackModules.getByProps('isFocused').isFocused() || document.getElementsByClassName('bda-slist').length) return;
                const isKeyBind = keybind => {
                    if (e.combo.length != keybind.length) return false;
                    // console.log(e.combo);
                    for (let i = 0; i < e.combo.length; i++) {
                        if (e.combo[i][1] != keybind[i]) {
                            return false;
                        }
                    }
                    return true;
                };
                const close = () => {
                    this.menu.filter = '';
                    this.menu.open = false;
                    ZLibrary.DiscordModules.ModalStack.popWithKey('ML2-MENU');
                };
                if (isKeyBind(this.settings.openLogKeybind)) {
                    if (this.menu.open) return close();
                    return this.openWindow();
                }
                if (isKeyBind(this.settings.openLogFilteredKeybind)) {
                    if (this.menu.open) return close();
                    if (!this.selectedChannel) {
                        this.showToast('No channel selected', { type: 'error' });
                        return this.openWindow();
                    }
                    this.menu.filter = `channel:${this.selectedChannel.id}`;
                    this.openWindow();
                }
            });
        };

        this.powerMonitor = ZLibrary.WebpackModules.getByProps('remotePowerMonitor').remotePowerMonitor;

        const refreshKeykindListener = () => {
            this.keybindListener.destroy();
            createKeybindListener();
        };

        this.keybindListenerInterval = setInterval(() => refreshKeykindListener(), 30 * 1000 * 60); // 10 minutes

        createKeybindListener();

        this.powerMonitor.on(
            'resume',
            (this.powerMonitorResumeListener = () => {
                setTimeout(() => refreshKeykindListener(), 1000);
            })
        );

        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.WebpackModules.getByDisplayName('TextAreaAutosize').prototype, 'focus', (thisObj, args, original) => {
                if (this.menu.open) return;
                return original(...args);
            })
        );

        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.WebpackModules.getByDisplayName('LazyImage').prototype, 'getSrc', (thisObj, args, original) => {
                let indx;
                if (((indx = thisObj.props.src.indexOf('?ML2=true')), indx !== -1)) return thisObj.props.src.substr(0, indx);
                return original(...args);
            })
        );

        this.dataManagerInterval = setInterval(() => {
            this.handleMessagesCap();
        }, 60 * 1000 * 5); // every 5 minutes, no need to spam it, could be intensive

        const ChannelContextMenu = ZLibrary.WebpackModules.getByDisplayName('ChannelContextMenu');
        const GuildContextMenu = ZLibrary.WebpackModules.getByDisplayName('GuildContextMenu');
        const MessageContextMenu = ZLibrary.WebpackModules.getByDisplayName('MessageContextMenu');
        const NativeLinkGroup = ZLibrary.WebpackModules.getByDisplayName('NativeLinkGroup');
        const UserContextMenu = ZLibrary.WebpackModules.getByDisplayName('UserContextMenu');

        this.ContextMenuItem = ZLibrary.DiscordModules.ContextMenuItem;
        this.ContextMenuActions = ZLibrary.DiscordModules.ContextMenuActions;
        this.ContextMenuGroup = ZLibrary.DiscordModules.ContextMenuItemsGroup;
        this.SubMenuItem = ZLibrary.WebpackModules.find(m => m.default && m.default.displayName && m.default.displayName.includes('SubMenuItem')).default;

        const patchContexts = menu => {
            this.unpatches.push(ZLibrary.Patcher.after(this.getName(), menu.prototype, 'render', (thisObj, args, returnValue) => this.handleContextMenu(thisObj, args, returnValue)));
        };

        for (let menu of [ChannelContextMenu, GuildContextMenu, MessageContextMenu, NativeLinkGroup, UserContextMenu]) patchContexts(menu);

        this.menu.randomValidChannel = (() => {
            const channels = ZLibrary.DiscordModules.ChannelStore.getChannels();
            var keys = Object.keys(channels);
            return channels[keys[(keys.length * Math.random()) << 0]];
        })();

        this.menu.userRequestQueue = [];

        this.menu.deleteKeyDown = false;
        document.addEventListener(
            'keydown',
            (this.keydownListener = e => {
                if (e.repeat) return;
                if (e.keyCode === 46) this.menu.deleteKeyDown = true;
            })
        );
        document.addEventListener(
            'keyup',
            (this.keyupListener = e => {
                if (e.repeat) return;
                if (e.keyCode === 46) this.menu.deleteKeyDown = false;
            })
        );

        this.menu.shownMessages = -1;
        const iconShit = ZLibrary.WebpackModules.getByProps('container', 'children', 'toolbar', 'iconWrapper');
        // Icon by font awesome
        // https://fontawesome.com/license
        this.channelLogButton = this.parseHTML(`<div tabindex="0" class="${iconShit.iconWrapper} ${iconShit.clickable}" id="ML2-OL" role="button" aria-label="Open logs">
                                                    <svg aria-hidden="true" class="${iconShit.icon}" name="Open logs" viewBox="0 0 576 512">
                                                        <path fill="currentColor" d="M218.17 424.14c-2.95-5.92-8.09-6.52-10.17-6.52s-7.22.59-10.02 6.19l-7.67 15.34c-6.37 12.78-25.03 11.37-29.48-2.09L144 386.59l-10.61 31.88c-5.89 17.66-22.38 29.53-41 29.53H80c-8.84 0-16-7.16-16-16s7.16-16 16-16h12.39c4.83 0 9.11-3.08 10.64-7.66l18.19-54.64c3.3-9.81 12.44-16.41 22.78-16.41s19.48 6.59 22.77 16.41l13.88 41.64c19.75-16.19 54.06-9.7 66 14.16 1.89 3.78 5.49 5.95 9.36 6.26v-82.12l128-127.09V160H248c-13.2 0-24-10.8-24-24V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24v-40l-128-.11c-16.12-.31-30.58-9.28-37.83-23.75zM384 121.9c0-6.3-2.5-12.4-7-16.9L279.1 7c-4.5-4.5-10.6-7-17-7H256v128h128v-6.1zm-96 225.06V416h68.99l161.68-162.78-67.88-67.88L288 346.96zm280.54-179.63l-31.87-31.87c-9.94-9.94-26.07-9.94-36.01 0l-27.25 27.25 67.88 67.88 27.25-27.25c9.95-9.94 9.95-26.07 0-36.01z"/>
                                                    </svg>
                                                </div>`);
        this.channelLogButton.addEventListener('click', () => {
            this.openWindow();
        });
        this.channelLogButton.addEventListener('contextmenu', () => {
            if (!this.selectedChannel) return;
            this.menu.filter = `channel:${this.selectedChannel.id}`;
            this.openWindow();
        });
        new ZeresPluginLibrary.EmulatedTooltip(this.channelLogButton, 'Open logs', { side: 'bottom' });

        if (this.settings.showOpenLogsButton) this.addOpenLogsButton();

        this.selfTestInterval = setInterval(() => {
            this.selfTestTimeout = setTimeout(() => {
                if (this.selfTestFailures > 4) {
                    clearInterval(this.selfTestInterval);
                    this.selfTestInterval = 0;
                    return BdApi.alert(`${this.getName()}: internal error.`, `Failed to hook dispatch. Recommended to reload your discord (CTRL + R) as the plugin may be in a broken state! If you still see this error, open up the devtools console (CTRL + SHIFT + I, click console tab) and report the errors to ${this.getAuthor()} for further assistance.`);
                }
                ZLibrary.Logger.warn(this.getName(), 'Dispatch is not hooked, all our hooks may be invalid, attempting to reload self');
                this.selfTestFailures++;
                this.stop();
                this.start();
            }, 3000);
            ZLibrary.WebpackModules.find(m => m.dispatch).dispatch({
                type: 'MESSAGE_LOGGER_V2_SELF_TEST'
            });
        }, 10 * 60 * 1000);

        if (this.selfTestInited) return;
        this.selfTestFailures = 0;
        this.selfTestInited = true;
    }
    shutdown() {
        const tryUnpatch = fn => {
            try {
                // things can bug out, best to reload tbh, should maybe warn the user?
                fn();
            } catch (e) {
                ZLibrary.Logger.stacktrace(this.getName(), 'Error unpatching', e);
            }
        };
        for (let unpatch of this.unpatches) tryUnpatch(unpatch);
        if (this.MessageContextMenuPatch) tryUnpatch(this.MessageContextMenuPatch);
        if (this.ChannelContextMenuPatch) tryUnpatch(this.ChannelContextMenuPatch);
        if (this.GuildContextMenuPatch) tryUnpatch(this.GuildContextMenuPatch);
        if (this.keybindListener) this.keybindListener.destroy();
        if (this.style && this.style.css) ZLibrary.PluginUtilities.removeStyle(this.style.css);
        if (this.dataManagerInterval) clearInterval(this.dataManagerInterval);
        if (this.keybindListenerInterval) clearInterval(this.keybindListenerInterval);
        if (this.selfTestInterval) clearInterval(this.selfTestInterval);
        if (this.selfTestTimeout) clearTimeout(this.selfTestTimeout);
        if (this.keydownListener) document.removeEventListener('keydown', this.keydownListener);
        if (this.keyupListener) document.removeEventListener('keyup', this.keyupListener);
        if (this.powerMonitor) this.powerMonitor.removeListener('resume', this.powerMonitorResumeListener);
        if (this.channelLogButton) this.channelLogButton.remove();
        // console.log('invalidating cache');
        this.invalidateAllChannelCache();
        //  if (this.selectedChannel) this.cacheChannelMessages(this.selectedChannel.id); // bad idea?
    }
    observer({ addedNodes }) {
        // from NoDeleteMessages by Mega_Mewthree (original), ShiiroSan (edit logging)
        let isChat = false;
        for (const change of addedNodes) {
            if ((isChat = typeof change.className === 'string' && change.className.indexOf(this.observer.chatClass) != -1) || (change.style && change.style.cssText === 'border-radius: 2px; background-color: rgba(114, 137, 218, 0);')) {
                try {
                    if (isChat) this.selectedChannel = this.getSelectedTextChannel();
                    if (!this.selectedChannel) return ZLibrary.Logger.warn(this.getName(), 'Chat was loaded but no text channel is selected');
                    if (isChat && this.settings.showOpenLogsButton) {
                        const srch = change.querySelector('div[class*="search-"]');
                        srch.parentElement.insertBefore(this.channelLogButton, srch);
                    }
                    this.updateMessages();
                    const showStuff = (map, name) => {
                        if (map[this.selectedChannel.id] && map[this.selectedChannel.id]) {
                            this.showToast(`There are ${map[this.selectedChannel.id]} new ${name} messages in ${this.selectedChannel.name ? '#' + this.selectedChannel.name : 'DM'}`, {
                                type: 'info',
                                onClick: () => this.openWindow(name),
                                timeout: 3000
                            });
                            map[this.selectedChannel.id] = 0;
                        }
                    };
                    if (this.settings.showDeletedCount) showStuff(this.deletedChatMessagesCount, 'deleted');
                    if (this.settings.showEditedCount) showStuff(this.editedChatMessagesCount, 'edited');
                } catch (e) {
                    ZLibrary.Logger.stacktrace(this.getName(), 'Error in observer', e);
                }
                break;
            }
        }
    }
    buildSetting(data) {
        // compied from ZLib actually
        const { name, note, type, value, onChange, id } = data;
        let setting = null;
        if (type == 'color') {
            setting = new ZLibrary.Settings.ColorPicker(name, note, value, onChange, { disabled: data.disabled }); // DOESN'T WORK, REEEEEEEEEE
        } else if (type == 'dropdown') {
            setting = new ZLibrary.Settings.Dropdown(name, note, value, data.options, onChange);
        } else if (type == 'file') {
            setting = new ZLibrary.Settings.FilePicker(name, note, onChange);
        } else if (type == 'keybind') {
            setting = new ZLibrary.Settings.Keybind(name, note, value, onChange);
        } else if (type == 'radio') {
            setting = new ZLibrary.Settings.RadioGroup(name, note, value, data.options, onChange, { disabled: data.disabled });
        } else if (type == 'slider') {
            const options = {};
            if (typeof data.markers != 'undefined') options.markers = data.markers;
            if (typeof data.stickToMarkers != 'undefined') options.stickToMarkers = data.stickToMarkers;
            setting = new ZLibrary.Settings.Slider(name, note, data.min, data.max, value, onChange, options);
        } else if (type == 'switch') {
            setting = new ZLibrary.Settings.Switch(name, note, value, onChange, { disabled: data.disabled });
        } else if (type == 'textbox') {
            setting = new ZLibrary.Settings.Textbox(name, note, value, onChange, { placeholder: data.placeholder || '' });
        }
        if (id) setting.id = id;
        return setting;
    }
    createSetting(data) {
        const current = Object.assign({}, data);
        if (!current.onChange) {
            current.onChange = value => {
                this.settings[current.id] = value;
                if (current.callback) current.callback(value);
            };
        }
        if (typeof current.value === 'undefined') current.value = this.settings[current.id];
        return this.buildSetting(current);
    }
    createGroup(group) {
        const { name, id, collapsible, shown, settings } = group;

        const list = [];
        for (let s = 0; s < settings.length; s++) list.push(this.createSetting(settings[s]));

        const settingGroup = new ZLibrary.Settings.SettingGroup(name, { shown, collapsible }).append(...list);
        settingGroup.id = id; // should generate the id in here instead?
        return settingGroup;
    }
    getSettingsPanel() {
        // todo, sort out the menu
        const list = [];
        list.push(
            this.createGroup({
                name: 'Keybinds',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Open menu keybind',
                        id: 'openLogKeybind',
                        type: 'keybind'
                    },
                    {
                        name: 'Open log filtered by selected channel',
                        id: 'openLogFilteredKeybind',
                        type: 'keybind'
                    },
                    {
                        name: 'Disable keybinds',
                        id: 'disableKeybind',
                        type: 'switch'
                    }
                ]
            })
        );
        list.push(
            this.createGroup({
                name: 'Ignores and overrides',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Ignore muted servers',
                        id: 'ignoreMutedGuilds',
                        type: 'switch'
                    },
                    {
                        name: 'Ignore muted channels',
                        id: 'ignoreMutedChannels',
                        type: 'switch'
                    },
                    {
                        name: 'Ignore bots',
                        id: 'ignoreBots',
                        type: 'switch'
                    },
                    {
                        name: 'Ignore messages posted by you',
                        id: 'ignoreSelf',
                        type: 'switch'
                    },
                    {
                        name: 'Ignore blocked users',
                        id: 'ignoreBlockedUsers',
                        type: 'switch'
                    },
                    {
                        name: 'Ignore NSFW channels',
                        id: 'ignoreNSFW',
                        type: 'switch'
                    },
                    {
                        name: 'Only log whitelist',
                        id: 'onlyLogWhitelist',
                        type: 'switch'
                    },
                    {
                        name: 'Always log selected channel, regardless of blacklist/whitelist',
                        id: 'alwaysLogSelected',
                        type: 'switch'
                    },
                    {
                        name: 'Always log DMs, regardless of blacklist/whitelist',
                        id: 'alwaysLogDM',
                        type: 'switch'
                    }
                ]
            })
        );
        list.push(
            this.createGroup({
                name: 'Display settings',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Display dates with timestamps',
                        id: 'displayDates',
                        type: 'switch',
                        callback: () => {
                            if (this.selectedChannel) {
                                // change NOW
                                this.invalidateAllChannelCache();
                                this.cacheChannelMessages(this.selectedChannel.id);
                            }
                        }
                    },
                    {
                        name: 'Display deleted messages in chat',
                        id: 'showDeletedMessages',
                        type: 'switch',
                        callback: () => {
                            this.invalidateAllChannelCache();
                            if (this.selectedChannel) this.cacheChannelMessages(this.selectedChannel.id);
                        }
                    },
                    {
                        name: 'Display edited messages in chat',
                        id: 'showEditedMessages',
                        type: 'switch'
                    },
                    {
                        name: 'Max number of shown edits',
                        id: 'maxShownEdits',
                        type: 'textbox',
                        onChange: val => {
                            if (isNaN(val)) return this.showToast('Value must be a number!', { type: 'error' });
                            this.settings.maxShownEdits = parseInt(val);
                        }
                    },
                    {
                        name: 'Show oldest edit instead of newest if over the shown edits limit',
                        id: 'hideNewerEditsFirst',
                        type: 'switch'
                    },
                    {
                        name: 'Display purged messages in chat',
                        id: 'showPurgedMessages',
                        type: 'switch',
                        callback: () => {
                            this.invalidateAllChannelCache();
                            if (this.selectedChannel) this.cacheChannelMessages(this.selectedChannel.id);
                        }
                    },
                    {
                        name: 'Restore deleted messages after reload',
                        id: 'restoreDeletedMessages',
                        type: 'switch',
                        callback: val => {
                            if (val) {
                                this.invalidateAllChannelCache();
                                if (this.selectedChannel) this.cacheChannelMessages(this.selectedChannel.id);
                            }
                        }
                    },
                    {
                        name: 'Show amount of new deleted messages when entering a channel',
                        id: 'showDeletedCount',
                        type: 'switch'
                    },
                    {
                        name: 'Show amount of new edited messages when entering a channel',
                        id: 'showEditedCount',
                        type: 'switch'
                    },
                    {
                        name: 'Display update notes',
                        id: 'displayUpdateNotes',
                        type: 'switch'
                    },
                    {
                        name: 'Menu sort direction',
                        id: 'reverseOrder',
                        type: 'radio',
                        options: [
                            {
                                name: 'New - old',
                                value: false
                            },
                            {
                                name: 'Old - new',
                                value: true
                            }
                        ]
                    }
                ]
            })
        );
        list.push(
            this.createGroup({
                name: 'Misc settings',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Disable saving data. Logged messages are erased after reload/restart. Disables auto backup.',
                        id: 'dontSaveData',
                        type: 'switch',
                        callback: val => {
                            if (!val) this.saveData();
                            if (!val && this.settings.autoBackup) this.saveBackup();
                        }
                    },
                    {
                        name: "Auto backup data (won't fully prevent losing data, just prevent total data loss)",
                        id: 'autoBackup',
                        type: 'switch',
                        callback: val => {
                            if (val && !this.settings.dontSaveData) this.saveBackup();
                        }
                    } /*
                    {
                        // no time, TODO!
                        name: 'Deleted messages color',
                        id: 'deletedMessageColor',
                        type: 'color'
                    }, */,
                    {
                        name: 'Aggresive message caching (makes sure we have the data of any deleted or edited messages)',
                        id: 'aggresiveMessageCaching',
                        type: 'switch'
                    },
                    {
                        name: 'Cache all images by storing them locally in the MLV2_IMAGE_CACHE folder inside the plugins folder',
                        id: 'cacheAllImages',
                        type: 'switch'
                    },
                    {
                        name: 'Display open logs button next to the search box top right in channels',
                        id: 'showOpenLogsButton',
                        type: 'switch',
                        callback: val => {
                            if (val) return this.addOpenLogsButton();
                            this.removeOpenLogsButton();
                        }
                    }
                ]
            })
        );
        list.push(
            this.createGroup({
                name: 'Toast notifications for guilds',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Message sent',
                        id: 'sent',
                        type: 'switch',
                        value: this.settings.toastToggles.sent,
                        onChange: val => {
                            this.settings.toastToggles.sent = val;
                        }
                    },
                    {
                        name: 'Message edited',
                        id: 'edited',
                        type: 'switch',
                        value: this.settings.toastToggles.edited,
                        onChange: val => {
                            this.settings.toastToggles.edited = val;
                        }
                    },
                    {
                        name: 'Message deleted',
                        id: 'deleted',
                        type: 'switch',
                        value: this.settings.toastToggles.deleted,
                        onChange: val => {
                            this.settings.toastToggles.deleted = val;
                        }
                    },
                    {
                        name: 'Ghost pings',
                        id: 'ghostPings',
                        type: 'switch',
                        value: this.settings.toastToggles.ghostPings,
                        onChange: val => {
                            this.settings.toastToggles.ghostPings = val;
                        }
                    },
                    {
                        name: 'Disable toasts for local user (yourself)',
                        id: 'disableToastsForLocal',
                        type: 'switch',
                        value: this.settings.toastToggles.disableToastsForLocal,
                        onChange: val => {
                            this.settings.toastToggles.disableToastsForLocal = val;
                        }
                    }
                ]
            })
        );

        list.push(
            this.createGroup({
                name: 'Toast notifications for DMs',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Message sent',
                        id: 'sent',
                        type: 'switch',
                        value: this.settings.toastTogglesDMs.sent,
                        onChange: val => {
                            this.settings.toastTogglesDMs.sent = val;
                        }
                    },
                    {
                        name: 'Message edited',
                        id: 'edited',
                        type: 'switch',
                        value: this.settings.toastTogglesDMs.edited,
                        onChange: val => {
                            this.settings.toastTogglesDMs.edited = val;
                        }
                    },
                    {
                        name: 'Message deleted',
                        id: 'deleted',
                        type: 'switch',
                        value: this.settings.toastTogglesDMs.deleted,
                        onChange: val => {
                            this.settings.toastTogglesDMs.deleted = val;
                        }
                    },
                    {
                        name: 'Ghost pings',
                        id: 'ghostPings',
                        type: 'switch',
                        value: this.settings.toastTogglesDMs.ghostPings,
                        onChange: val => {
                            this.settings.toastTogglesDMs.ghostPings = val;
                        }
                    }
                ]
            })
        );

        list.push(
            this.createGroup({
                name: 'Message caps',
                id: this.randomString(),
                collapsible: true,
                shown: false,
                settings: [
                    {
                        name: 'Cached messages cap',
                        note: 'Max number of sent messages logger should keep track of',
                        id: 'messageCacheCap',
                        type: 'textbox',
                        onChange: val => {
                            if (isNaN(val)) return this.showToast('Value must be a number!', { type: 'error' });
                            this.settings.messageCacheCap = parseInt(val);
                            clearInterval(this.dataManagerInterval);
                            this.dataManagerInterval = setInterval(() => {
                                this.handleMessagesCap();
                            }, 60 * 1000 * 5);
                        }
                    },
                    {
                        name: 'Saved messages cap',
                        note: 'Max number of messages saved to disk',
                        id: 'savedMessagesCap',
                        type: 'textbox',
                        onChange: val => {
                            if (isNaN(val)) return this.showToast('Value must be a number!', { type: 'error' });
                            this.settings.savedMessagesCap = parseInt(val);
                            clearInterval(this.dataManagerInterval);
                            this.dataManagerInterval = setInterval(() => {
                                this.handleMessagesCap();
                            }, 60 * 1000 * 5);
                        }
                    },
                    {
                        name: 'Menu message render cap',
                        note: 'How many messages will show before the LOAD MORE button will show',
                        id: 'renderCap',
                        type: 'textbox',
                        onChange: val => {
                            if (isNaN(val)) return this.showToast('Value must be a number!', { type: 'error' });
                            this.settings.renderCap = parseInt(val);
                            clearInterval(this.dataManagerInterval);
                        }
                    }
                ]
            })
        );
        const div = document.createElement('div');
        div.style.display = 'inline-flex';
        div.appendChild(this.createButton('Changelog', () => ZLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges())));
        div.appendChild(this.createButton('Stats', () => this.showStatsModal()));
        div.appendChild(this.createButton('Donate', () => this.nodeModules.electron.shell.openExternal('https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DZUL9UZDDRLB8&source=url')));
        div.appendChild(this.createButton('Help', () => this.showLoggerHelpModal()));
        let button = div.firstElementChild;
        while (button) {
            button.style.marginRight = button.style.marginLeft = `5px`;
            button = button.nextElementSibling;
        }

        list.push(div);

        return ZLibrary.Settings.SettingPanel.build(_ => this.saveSettings(), ...list);
    }
    /* ==================================================-|| START HELPERS ||-================================================== */
    saveSettings() {
        ZLibrary.PluginUtilities.saveSettings(this.getName(), this.settings);
    }
    loadData(name, construct = {}) {
        try {
            return $.extend(true, construct, BdApi.getData(name, 'data')); // zeres library turns all arrays into objects.. for.. some reason?
        } catch (e) {
            return {}; // error handling not done here
        }
    }
    handleDataSaving() {
        // saveData/setPluginData is synchronous, can get slow with bigger files
        ZLibrary.PluginUtilities.saveData(this.getName() + 'Data', 'data', {
            messageRecord: this.messageRecord,
            deletedMessageRecord: this.deletedMessageRecord,
            editedMessageRecord: this.editedMessageRecord,
            purgedMessageRecord: this.purgedMessageRecord
        });
        if (this.settings.autoBackup) {
            if (this.saveBackupTimeout) this.autoBackupSaveInterupts++;
            if (this.autoBackupSaveInterupts < 4) {
                if (this.saveBackupTimeout) clearTimeout(this.saveBackupTimeout);
                // 10 seconds after, in case shits going down y'know, better not to spam save and corrupt it, don't become the thing you're trying to eliminate
                this.saveBackupTimeout = setTimeout(() => this.saveBackup(), 10 * 1000);
            }
        }
        this.requestedDataSave = 0;
    }
    saveData() {
        if (!this.settings.dontSaveData && !this.requestedDataSave) this.requestedDataSave = setTimeout(() => this.handleDataSaving(), 1000); // needs to be async
        this.updateMessages();
    }
    saveBackup() {
        this.saveBackupTimeout = 0;
        this.autoBackupSaveInterupts = 0;
        ZLibrary.PluginUtilities.saveData(this.getName() + 'DataBackup', 'data', {
            messageRecord: this.messageRecord,
            deletedMessageRecord: this.deletedMessageRecord,
            editedMessageRecord: this.editedMessageRecord,
            purgedMessageRecord: this.purgedMessageRecord
        });
        if (!this.loadData(this.getName() + 'DataBackup', 'data').messageRecord) this.saveBackupTimeout = setTimeout(() => this.saveBackup, 300); // don't be taxing
    }
    parseHTML(html) {
        // TODO: drop this func, it's 75% slower than just making the elements manually
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }
    randomString() {
        let start = rand();
        while (start[0].toUpperCase() == start[0].toLowerCase()) start = rand();
        return start + '-' + rand();
        function rand() {
            return Math.random()
                .toString(36)
                .substr(2, 7);
        }
    }
    createTimeStamp(from = undefined, forcedDate = false) {
        // todo: timestamp for edited tooltip
        let date;
        if (from) date = new Date(from);
        else date = new Date();
        return this.settings.displayDates || forcedDate ? `${date.toLocaleTimeString()}, ${date.toLocaleDateString()}` : date.toLocaleTimeString();
    }
    getCachedMessage(id, channelId = 0) {
        let cached = this.cachedMessageRecord.find(m => m.id == id);
        if (cached) return cached;
        if (channelId) return this.tools.getMessage(channelId, id); // if the message isn't cached, it returns undefined
        return null;
    }
    getEditedMessage(messageId, channelId) {
        if (this.editedMessageRecord[channelId] && this.editedMessageRecord[channelId].findIndex(m => m === messageId) != -1) {
            return this.messageRecord[messageId];
        }
        return null;
    }
    getSavedMessage(id) {
        /* DEPRECATED */
        return this.messageRecord[id];
    }
    createMiniFormattedData(message) {
        message = $.extend(true, {}, message);
        let internalMessage = this.tools.getMessage(message.channel_id, message.id);
        let reactions = internalMessage ? internalMessage.reactions : [];
        let guildId = null;
        /* let guild = null; */
        /* const author = this.tools.getUser(message.author.id); */
        const channel = this.tools.getChannel(message.channel_id);
        if (channel) guildId = channel.guild_id;
        /* if (guildId) guild = this.tools.getServer(guildId); */
        const obj = {
            message: {
                author: {
                    discriminator: message.author.discriminator,
                    username: message.author.username,
                    avatar: message.author.avatar,
                    id: message.author.id,
                    bot: internalMessage && internalMessage.author && internalMessage.author.bot // TODO: is bot checking in menu
                },
                /* channel_name: (channel && channel.name) || 'unknown-channel',
                guild_name: (guild && guild.name) || 'unknown-guild', */
                mention_everyone: message.mention_everyone,
                edited_timestamp: message.edited_timestamp,
                mention_roles: message.mention_roles,
                attachments: message.attachments,
                channel_id: message.channel_id,
                timestamp: message.timestamp,
                mentions: message.mentions,
                content: message.content,
                pinned: message.pinned,
                embeds: message.embeds,
                reactions: reactions,
                type: message.type,
                guild_id: guildId,
                tts: message.tts,
                id: message.id
            },
            local_mentioned: this.tools.isMentioned(message, this.localUser.id),
            delete_data: null /*  {
                time: integer,
                rel_ids: [
                    string,
                    string,
                    string
                ],
                hidden: bool
            } */,
            edit_history: null /* [
                {
                    content: string,
                    timestamp: string
                }
            ],
            edits_hidden: bool */
        };
        this.fixEmbeds(obj.message);
        return obj;
    }
    getSelectedTextChannel() {
        return ZLibrary.DiscordModules.ChannelStore.getChannel(ZLibrary.DiscordModules.SelectedChannelStore.getChannelId());
    }
    invalidateAllChannelCache() {
        for (let channelId in this.channelMessages) this.invalidateChannelCache(channelId);
    }
    invalidateChannelCache(channelId) {
        if (!this.channelMessages[channelId]) return;
        this.channelMessages[channelId].ready = false;
    }
    cacheChannelMessages(id, relative) {
        // TODO figure out if I can use this to get messages at a certain point
        this.tools.fetchMessages(id, null, null, 50, (relative && { messageId: relative, ML2: true }) || undefined);
    }
    cachenChannelMessagesRelative(channelId, messageId) {
        ZLibrary.DiscordModules.APIModule.get({
            url: ZLibrary.DiscordModules.DiscordConstants.Endpoints.MESSAGES(channelId),
            query: {
                before: null,
                after: null,
                limit: 50,
                around: messageId
            }
        })
            .then(res => {
                if (res.status != 200) return;
                const results = res.body;
                const final = results.filter(x => this.cachedMessageRecord.findIndex(m => x.id === m.id) == -1);
                this.cachedMessageRecord.push(...final);
            })
            .catch(err => {
                ZLibrary.Logger.stacktrace(this.getName(), `Erro caching messages from ${channelId} around ${messageId}`, err);
            });
    }
    formatMarkup(content, channelId) {
        const markup = document.createElement('div');

        const parsed = this.tools.parse(content, true, channelId ? { channelId: channelId } : {});
        // console.log(parsed);
        // error, this render doesn't work with tags
        //  TODO: this parser and renderer sucks
        ZLibrary.DiscordModules.ReactDOM.render(ZLibrary.DiscordModules.React.createElement('div', { className: '' }, parsed), markup);

        const hiddenClass = this.classes.hidden;

        const hidden = markup.getElementsByClassName(hiddenClass);

        for (let i = 0; i < hidden.length; i++) {
            hidden[i].classList.remove(hiddenClass);
        }

        return markup;
    }
    async showToast(content, options = {}) {
        // credits to Zere, copied from Zeres Plugin Library
        const { type = '', icon = '', timeout = 3000, onClick = () => {}, onContext = () => {} } = options;
        ZLibrary.Toasts.ensureContainer();
        const toast = ZLibrary['DOMTools'].parseHTML(ZLibrary.Toasts.buildToast(content, ZLibrary.Toasts.parseType(type), icon));
        document.querySelector('.toasts').appendChild(toast);
        const wait = () => {
            toast.classList.add('closing');
            setTimeout(() => {
                toast.remove();
                if (!document.querySelectorAll('.toasts .toast').length) document.querySelector('.toasts').remove();
            }, 300);
        };
        const sto = setTimeout(wait, timeout);
        const toastClicked = () => {
            clearTimeout(sto);
            wait();
        };
        toast.addEventListener('auxclick', () => toastClicked());
        toast.addEventListener('click', () => {
            toastClicked();
            onClick();
        });
        toast.addEventListener('contextmenu', () => {
            toastClicked();
            onContext();
        });
    }
    clamp(val, min, max) {
        // this is so sad, can we hit Metalloriff?
        // his message logger added the func to Math obj and I didn't realize
        return Math.max(min, Math.min(val, max));
    }
    deleteEditedMessageFromRecord(id, editNum) {
        const record = this.messageRecord[id];
        if (!record) return;

        record.edit_history.splice(editNum, 1);
        if (!record.edit_history.length) record.edit_history = null;
        else return this.saveData();

        const channelId = record.message.channel_id;
        const channelMessages = this.editedMessageRecord[channelId];
        channelMessages.splice(channelMessages.findIndex(m => m === id), 1);
        if (this.deletedMessageRecord[channelId] && this.deletedMessageRecord[channelId].findIndex(m => m === id) != -1) return this.saveData();
        if (this.purgedMessageRecord[channelId] && this.purgedMessageRecord[channelId].findIndex(m => m === id) != -1) return this.saveData();
        delete this.messageRecord[id];
        this.saveData();
    }
    jumpToMessage(channelId, messageId, guildId) {
        if (this.menu.open) ZLibrary.DiscordModules.ModalStack.popWithKey('ML2-MENU');
        ZLibrary.DiscordModules.NavigationUtils.transitionTo(`/channels/${guildId || '@me'}/${channelId}${messageId ? '/' + messageId : ''}`);
    }
    isImage(url) {
        return /\.(jpe?g|png|gif|bmp)$/i.test(url);
    }
    fixEmbeds(message) {
        if (!this.fixEmbeds.hex2int) this.fixEmbeds.hex2int = ZLibrary.WebpackModules.getByProps('hex2int').hex2int;

        for (let embed of message.embeds) {
            if (typeof embed.title !== 'string') embed.title = embed.rawTitle || '';
            if (typeof embed.description !== 'string') embed.description = embed.rawDescription || '';
            if (typeof embed.color === 'string') embed.color = this.fixEmbeds.hex2int(embed.color);
            if (typeof embed.footer === 'object')
                embed.footer = {
                    text: embed.footer.text,
                    icon_url: embed.footer.iconURL,
                    proxy_icon_url: embed.footer.iconProxyURL
                };
            if (typeof embed.author === 'object')
                embed.author = {
                    name: embed.author.name,
                    url: embed.author.url,
                    icon_url: embed.author.iconURL,
                    proxy_icon_url: embed.author.iconProxyURL
                };
            if (embed.referenceId) embed.reference_id = embed.referenceId;
            if (embed.fields && embed.fields.length) {
                for (let field of embed.fields) {
                    for (let b in field) {
                        if (Array.isArray(field[b])) {
                            let result = '';
                            for (let c of field[b]) {
                                if (typeof c === 'string') result += c;
                                else if (c.props && c.props.text) result += c.props.text;
                                else if (c.props && c.props.title && c.props.href) result += `[${c.props.title}](${c.props.href})`;
                            }
                            field[b] = result;
                        }
                    }
                }
            }
        }
    }
    /* ==================================================-|| END HELPERS ||-================================================== */
    /* ==================================================-|| START MISC ||-================================================== */
    addOpenLogsButton() {
        if (!this.selectedChannel) return;
        const parent = document.querySelector('div[class*="chat-"] div[class*="toolbar-"]');
        parent.insertBefore(this.channelLogButton, parent.querySelector('div[class*="search-"]'));
    }
    removeOpenLogsButton() {
        const button = document.getElementById('ML2-OL');
        button.remove();
    }
    showLoggerHelpModal() {
        this.createModal({
            confirmText: 'OK',
            header: 'Logger help',
            size: ZLibrary.Modals.ModalSizes.LARGE,
            children: [
                ZLibrary.ReactTools.createWrappedElement([
                    this.parseHTML(
                        `<div class="${this.multiClasses.defaultColor}">
                            <strong>Menu:</strong></br>
                            <div class="ml2-help-text-indent">
                                DELETE + LEFT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Clicking on a message, deletes the message</br>
                                    Clicking on an edit deletes that specific edit</br>
                                    Clicking on the timestamp deletes all messages in that message group
                                </div></br>
                                RIGHT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Right-clicking the timestamp opens up options for the entire message group
                                </div></br>
                            </div>
                            <strong>Toasts:</strong></br>
                            <div class="ml2-help-text-indent">
                                Note: Little "notifications" in discord that tell you if a message was edited, deleted, purged etc are called Toasts!</br></br>
                                LEFT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Opens menu with the relevant tab</br>
                                </div></br>
                                RIGHT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Jumps to relevant message in the relevant channel
                                </div></br>
                                MIDDLE-CLICK/SCROLLWHEEL-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Only dismisses/closes the Toast.
                                </div></br>
                            </div>
                            <strong>Open logs button (top right next to search):</strong></br>
                            <div class="ml2-help-text-indent">
                                LEFT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Opens menu</br>
                                </div></br>
                                RIGHT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Opens filtered menu that only shows messages from selected channel</br>
                                </div></br>
                            </div>
                            <strong>Whitelist/blacklist, ignores and overrides:</strong></br>
                            <div class="ml2-help-text-indent">
                                WHITELIST-ONLY:</br>
                                <div class="ml2-help-text-indent">
                                    All servers are ignored unless whitelisted</br>
                                    Muted channels in whitelisted servers are ignored unless whitelisted or "Ignore muted channels" is disabled</br>
                                    All channels in whitelisted servers are logged unless blacklisted, or muted and "Ignore muted channels" is enabled
                                </div></br>
                                DEFAULT:</br>
                                <div class="ml2-help-text-indent">
                                    All servers are logged unless blacklisted or muted and "Ignore muted servers" is enabled</br>
                                    Muted channels are ignored unless whitelisted or "Ignore muted channels" is disabled</br>
                                    Muted servers are ignored unless whitelisted or "Ignore muted servers" is disabled</br>
                                    Whitelisted channels in muted or blacklisted servers are logged</br>
                                </div></br>
                                ALL:</br>
                                <div class="ml2-help-text-indent">
                                    Whitelisted channels in blacklisted servers are logged</br>
                                    Blacklisted channels in whitelisted servers are ignored</br>
                                    "Always log selected channel" overrides blacklist, whitelist-only mode, NSFW channel ignore, mute</br>
                                    "Always log DMs" overrides blacklist as well as whitelist-only mode</br>
                                    Channels marked NSFW and not whitelisted are ignored unless "Ignore NSFW channels" is disabled
                                </div></br>
                            </div>
                            <strong>Chat:</strong></br>
                            <div class="ml2-help-text-indent">
                                LEFT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Clicking the (Edited) text opens menu of specified message
                                </div></br>
                                RIGHT-CLICK:</br>
                                <div class="ml2-help-text-indent">
                                    Right-clicking an edit (darkened text) allows you to delete that edit, or hide edits</br>
                                    Right-clicking on a edited or deleted message gives you the option to hide the deleted message or hide or unhide edits.
                                </div></br>
                            </div>
                        </div>`
                    )
                ])
            ],
            red: false
        });
    }
    showStatsModal() {
        const elements = [];
        let totalMessages = Object.keys(this.messageRecord).length;
        let messageCounts = [];
        let spaceUsageMB = 0;
        let cachedImageCount = 0;
        let cachedImagesUsageMB = 0;

        let mostDeletesChannel = { num: 0, id: '' };
        let mostEditsChannel = { num: 0, id: '' };
        let deleteDataTemp = {};
        let editDataTemp = {};

        for (const map of [this.deletedMessageRecord, this.editedMessageRecord, this.cachedMessageRecord]) {
            let messageCount = 0;
            if (!Array.isArray(map)) {
                for (const channelId in map) {
                    if (!deleteDataTemp[channelId]) deleteDataTemp[channelId] = [];
                    if (!editDataTemp[channelId]) editDataTemp[channelId] = [];
                    for (const messageId of map[channelId]) {
                        messageCount++;
                        const record = this.messageRecord[messageId];
                        if (!record) continue; // wtf?
                        if (record.delete_data && deleteDataTemp[channelId].findIndex(m => m === messageId)) deleteDataTemp[channelId].push(messageId);
                        if (record.edit_history && editDataTemp[channelId].findIndex(m => m === messageId)) editDataTemp[channelId].push(messageId);
                    }
                }
            }
            for (const channelId in deleteDataTemp) if (deleteDataTemp[channelId].length > mostDeletesChannel.num) mostDeletesChannel = { num: deleteDataTemp[channelId].length, id: channelId };
            for (const channelId in editDataTemp) if (editDataTemp[channelId].length > mostEditsChannel.num) mostEditsChannel = { num: editDataTemp[channelId].length, id: channelId };

            messageCounts.push(messageCount);
        }
        const addLine = (name, value) => {
            elements.push(this.parseHTML(`<div class="${this.multiClasses.defaultColor}"><strong>${name}</strong>: ${value}</div></br>`));
        };
        addLine('Total messages', totalMessages);
        addLine('Deleted message count', messageCounts[0]);
        addLine('Edited message count', messageCounts[1]);
        addLine('Sent message count', messageCounts[2]);

        let channel = this.tools.getChannel(mostDeletesChannel.id);
        if (channel) addLine('Most deletes', mostDeletesChannel.num + ' ' + this.getLiteralName(channel.guild_id, channel.id));
        if (channel) addLine('Most edits', mostEditsChannel.num + ' ' + this.getLiteralName(channel.guild_id, channel.id));
        this.createModal({
            confirmText: 'OK',
            header: 'Data stats',
            size: ZLibrary.Modals.ModalSizes.SMALL,
            children: [ZLibrary.ReactTools.createWrappedElement(elements)],
            red: false
        });
    }
    reAddDeletedMessages(messages, id, deletedMessages, purgedMessages) {
        // hack
        if (this.antiinfiniteloop.findIndex(m => m === id) != -1) {
            //this.showToast('Infinite loop detected in data! Faulty ID:' + id, { type: 'error', timeout: 5000 });
            //ZLibrary.Logger.err(this.getName(), `Infinite loop detected in data! Cancelled restore of ${id}!`);
            // todo: add code to prevent this bullshit
            return false;
        }
        const record = this.messageRecord[id];
        if (!record || !record.delete_data || record.delete_data.hidden || !record.delete_data.rel_ids) return false;

        this.antiinfiniteloop.push(record.message.id);

        for (let prev in record.delete_data.rel_ids) {
            let relID = record.delete_data.rel_ids[prev];
            let mIDX = (messages.length && messages.length - 1) || 0;
            let addIn = false;
            if (relID != 'CHANNELEND') {
                mIDX = messages.findIndex(m => m.id === relID);
            }
            if (mIDX !== -1) addIn = true;
            else {
                if (deletedMessages && deletedMessages.findIndex(m => m === relID) === -1 && (!this.settings.showPurgedMessages || !purgedMessages || purgedMessages.findIndex(m => m === relID) === -1)) continue;
                if (this.reAddDeletedMessages(messages, relID)) addIn = true;
            }
            if (addIn) {
                mIDX = messages.findIndex(m => m.id === relID);
                //let copy = Object.assign({}, record.message);
                //for (let attachmentIDX in copy.attachments) {
                //    let attachment = copy.attachments[attachmentIDX];
                //    console.log(attachment.proxy_url);
                //    attachment.proxy_url = this.imageCacheIdToDataURL(attachment.proxy_url);
                //    console.log(attachment.proxy_url);
                //}
                messages.splice(mIDX, 0, record.message);
                return true;
            }
        }
        return false;
    }
    getLiteralName(guildId, channelId, guildNameBackup = -1, channelNameBackup = -1) {
        // TODO, custom channel server failure text
        const guild = this.tools.getServer(guildId);
        const channel = this.tools.getChannel(channelId); // todo
        /* if (typeof guildNameBackup !== 'number' && guild && guildNameBackup)  */ if (guildId) {
            const channelName = channel ? channel.name : 'unknown-channel';
            const guildName = guild ? guild.name : 'unknown-server';
            return `${guildName}, #${channelName}`;
        } else if (channel && channel.name.length) {
            return `group ${channel.name}`;
        } else if (channel && channel.type == 3) {
            let finalGroupName = '';
            for (let i of channel.recipients) {
                const user = this.tools.getUser(i);
                if (!user) continue;
                finalGroupName += ',' + user.username;
            }
            if (!finalGroupName.length) {
                return 'unknown group';
            } else {
                finalGroupName = finalGroupName.substr(1);
                finalGroupName = finalGroupName.length > 10 ? finalGroupName.substr(0, 10 - 1) + '...' : finalGroupName;
                return `group ${finalGroupName}`;
            }
        } else if (channel && channel.recipients) {
            const user = this.tools.getUser(channel.recipients[0]);
            if (!user) return 'DM';
            return `${user.username} DM`;
        } else {
            return 'DM';
        }
    }
    getRelativeMessages(id, channelId) {
        const messages = this.channelMessages[channelId];
        // ready check may be redundant, discord may not save the message if the channel isn't ready
        if (messages && messages.ready && messages._array.length) {
            let foundMessage = false;
            let relMessages = [];
            for (let i = messages._array.length - 1; i >= 0; i--) {
                const message = messages._array[i];
                if (!foundMessage && message.id == id) {
                    foundMessage = true;
                    continue;
                }
                if (foundMessage) relMessages.push(message.id);
                if (relMessages.length >= 3) return relMessages;
            } // todo, better caching if hasMoreBefore = true
            if (!messages.hasMoreBefore) {
                relMessages.push('CHANNELEND');
                return relMessages;
            }
        }
        return null;
    }
    saveDeletedMessage(message, targetMessageRecord) {
        let result = this.createMiniFormattedData(message);
        result.delete_data = {};
        const id = message.id;
        const channelId = message.channel_id;
        result.delete_data.time = new Date().getTime();
        if (!(result.delete_data.rel_ids = this.getRelativeMessages(id, channelId))) {
            //console.error(`[${this.getName()}]: saveDeletedMessage -> Failed to get relative IDs!`);
            //if (this.settings.displayInChat) ZLibrary.Toasts.error(`Failed to get relative IDs! Deleted message will not show in chat after reload!`, { timeout: 7500 });
        }
        if (!Array.isArray(targetMessageRecord[channelId])) targetMessageRecord[channelId] = [];
        if (this.messageRecord[id]) {
            let record = this.messageRecord[id];
            record.delete_data = result.delete_data;
        } else {
            this.messageRecord[id] = result;
        }
        if (this.messageRecord[id].message.attachments) {
            const attachments = this.messageRecord[id].message.attachments;
            for (let i = 0; i < attachments.length; i++) {
                attachments[i].url = attachments[i].proxy_url; // proxy url lasts longer
            }
        }
        if (this.settings.cacheAllImages) this.cacheMessageImages(this.messageRecord[id].message);
        targetMessageRecord[channelId].push(id);
    }
    createButton(label, callback) {
        const classes = this.createButton.classes;
        const ret = this.parseHTML(`<button type="button" class="${classes.button}"><div class="${classes.buttonContents}">${label}</div></button>`);
        if (callback) ret.addEventListener('click', callback);
        return ret;
    }
    createModal(options, image, name) {
        const modal = image ? this.createModal.imageModal : this.createModal.confirmationModal;
        ZLibrary.DiscordModules.ModalStack.push(
            function(props) {
                return ZLibrary.DiscordModules.React.createElement(modal, Object.assign(options, props));
            },
            undefined,
            name
        );
    }
    getMessageAny(id) {
        const record = this.messageRecord[id];
        if (!record) return this.cachedMessageRecord.find(m => m.id == id);
        return record.message;
    }
    handleImageCacheDataSaving() {
        this.requestedImageCacheDataSave = 0;
        // saveData/setPluginData is synchronous, can get slow with bigger files
        ZLibrary.PluginUtilities.saveData('MLV2_IMAGE_CACHE/ImageCache', 'data', {
            imageCacheRecord: this.imageCacheRecord
        });
    }
    saveImageCacheData() {
        if (!this.settings.dontSaveData && !this.requestedImageCacheDataSave) this.requestedImageCacheDataSave = setTimeout(() => this.handleImageCacheDataSaving(), 5000); // needs to be async
    }
    cacheImage(url, attachmentIdx, attachmentId, messageId, channelId, attempts = 0) {
        this.nodeModules.request({ url: url, encoding: null }, (err, res, buffer) => {
            if (err || res.statusCode != 200) {
                if (res.statusCode == 404 || res.statusCode == 403) return;
                attempts++;
                if (attempts > 3) return ZLibrary.Logger.warn(this.getName(), `Failed to get image ${attachmentId} for caching, error code ${res.statusCode}`);
                return setTimeout(() => this.cacheImage(url, attachmentIdx, attachmentId, messageId, channelId, attempts), 1000);
            }
            const fileExtension = url.match(/\.[0-9a-z]+$/i)[0];
            this.nodeModules.fs.writeFileSync(this.imageCacheDir + `/${attachmentId}${fileExtension}`, buffer, { encoding: null });
            let reader = new FileReader();
            reader.readAsDataURL(new Blob([buffer], { type: 'image/png' }));
            reader.onload = () => {
                const img = new Image();
                const canvas = document.createElement('canvas');
                img.src = reader.result;
                img.onload = () => {
                    const ctx = canvas.getContext('2d');
                    let width = img.width;
                    let height = img.height;
                    const maxSize = 256;
                    if (width > maxSize || height > maxSize) {
                        if (width > height) {
                            const scale = maxSize / width;
                            width = maxSize;
                            height *= scale;
                        } else {
                            const scale = maxSize / height;
                            height = maxSize;
                            width *= scale;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, width, height);
                    let outputData = canvas.toDataURL('image/png');
                    outputData = outputData.substr(0, 14) + `;${channelId};${attachmentId}` + outputData.substr(14);
                    this.imageCacheRecord[attachmentId] = {
                        thumbnail: outputData,
                        attachmentIdx: attachmentIdx,
                        messageId: messageId,
                        extension: fileExtension
                    };
                    this.saveImageCacheData();
                };
            };
        });
    }
    cacheMessageImages(message) {
        // don't block it, ugly but works, might rework later
        setTimeout(() => {
            for (let i = 0; i < message.attachments.length; i++) {
                const attachment = message.attachments[i];
                if (!this.isImage(attachment.url)) continue;
                this.cacheImage(attachment.url, i, attachment.id, message.id, message.channel_id);
            }
        }, 0);
    }
    /* ==================================================-|| END MISC ||-================================================== */
    /* ==================================================-|| START MESSAGE MANAGMENT ||-================================================== */
    deleteMessageFromRecords(id) {
        const record = this.messageRecord[id];
        if (!record) {
            for (let map of [this.deletedMessageRecord, this.editedMessageRecord, this.purgedMessageRecord]) {
                for (let channelId in map) {
                    const index = map[channelId].findIndex(m => m === id);
                    if (index == -1) continue;
                    map[channelId].splice(index, 1);
                    if (!map[channelId].length) delete map[channelId];
                }
            }
            return;
        }
        // console.log('Deleting', record);
        const channelId = record.message.channel_id;
        for (let map of [this.deletedMessageRecord, this.editedMessageRecord, this.purgedMessageRecord]) {
            if (!map[channelId]) continue;
            const index = map[channelId].findIndex(m => m === id);
            if (index == -1) continue;
            map[channelId].splice(index, 1);
            if (!map[channelId].length) delete map[channelId];
        }
        delete this.messageRecord[id];
    }
    handleMessagesCap() {
        try {
            // TODO: add empty record and infinite loop checking for speed improvements
            const extractAllMessageIds = map => {
                let ret = [];
                for (let channelId in map) {
                    for (let messageId of map[channelId]) {
                        ret.push(messageId);
                    }
                }
                return ret;
            };
            if (this.cachedMessageRecord.length > this.settings.messageCacheCap) this.cachedMessageRecord.splice(0, this.cachedMessageRecord.length - this.settings.messageCacheCap);
            let changed = false;
            const deleteMessages = map => {
                this.sortMessagesByAge(map);
                const toDelete = map.length - this.settings.savedMessagesCap;
                for (let i = map.length - 1, deleted = 0; i >= 0 && deleted != toDelete; i--, deleted++) {
                    this.deleteMessageFromRecords(map[i]);
                }
                changed = true;
            };
            const handleInvalidEntries = map => {
                for (let channelId in map) {
                    for (let messageIdIdx = map[channelId].length - 1; messageIdIdx >= 0; messageIdIdx--) {
                        if (!Array.isArray(map[channelId])) {
                            delete map[channelId];
                            changed = true;
                            continue;
                        }
                        if (!this.messageRecord[map[channelId][messageIdIdx]]) {
                            map[channelId].splice(messageIdIdx, 1);
                            changed = true;
                        }
                    }
                    if (!map[channelId].length) {
                        delete map[channelId];
                        changed = true;
                    }
                }
            };
            const checkIsInRecords = (channelId, messageId) => {
                for (let map of [this.deletedMessageRecord, this.editedMessageRecord, this.purgedMessageRecord]) if (map[channelId] && map[channelId].findIndex(m => m === messageId) != -1) return true;
                return false;
            };

            for (let map of [this.deletedMessageRecord, this.editedMessageRecord, this.purgedMessageRecord]) handleInvalidEntries(map);
            for (let messageId in this.messageRecord) {
                if (!checkIsInRecords(this.messageRecord[messageId].message.channel_id, messageId)) delete this.messageRecord[messageId];
            }
            let deletedMessages = extractAllMessageIds(this.deletedMessageRecord);
            let editedMessages = extractAllMessageIds(this.editedMessageRecord);
            let purgedMessages = extractAllMessageIds(this.purgedMessageRecord);
            for (let map of [deletedMessages, editedMessages, purgedMessages]) if (map.length > this.settings.savedMessagesCap) deleteMessages(map);
            if (changed) this.saveData();
            if (!this.settings.cacheAllImages) return;
            changed = false;
            for (let attId in this.imageCacheRecord) {
                const record = this.imageCacheRecord[attId];
                const imagePath = `${this.imageCacheDir}/${attId}${record.extension}`;
                if (this.messageRecord[record.messageId] && this.nodeModules.fs.existsSync(imagePath)) continue;
                delete this.imageCacheRecord[attId];
                changed = true;
            }
            if (changed) this.saveImageCacheData();
            const savedImages = this.nodeModules.fs.readdirSync(this.imageCacheDir);
            for (let img of savedImages) {
                if (img.indexOf('ImageCache.config.json') != -1) continue;
                const attId = img.match(/(\d*).[a-z]+/i)[1];
                if (this.imageCacheRecord[attId]) continue;
                try {
                    this.nodeModules.fs.unlinkSync(`${this.imageCacheDir}/${img}`);
                } catch (e) {
                    ZLibrary.Logger.err(this.getName(), 'Error deleting unreferenced image, what the shit', e);
                }
            }
        } catch (e) {
            ZLibrary.Logger.stacktrace(this.getName(), 'Error clearing out data', e);
        }
    }
    /* ==================================================-|| END MESSAGE MANAGMENT ||-================================================== */
    onDispatchEvent(args, callDefault) {
        const dispatch = args[0];

        if (!dispatch) return callDefault(...args);

        try {
            if (dispatch.type === 'MESSAGE_LOGGER_V2_SELF_TEST') {
                clearTimeout(this.selfTestTimeout);
                //console.log('Self test OK');
                this.selfTestFailures = 0;
                return;
            }
            // if (dispatch.type == 'EXPERIMENT_TRIGGER') return callDefault(...args);
            // console.log('INFO: onDispatchEvent -> dispatch', dispatch);
            if (dispatch.type === 'CHANNEL_SELECT') {
                callDefault(...args);
                this.selectedChannel = this.getSelectedTextChannel();

                return;
            }

            if (dispatch.type === 'MODAL_POP') {
                callDefault(...args);
                if (!ZLibrary.WebpackModules.getByProps('isModalOpenWithKey').isModalOpenWithKey('ML2-MENU')) {
                    this.menu.filter = '';
                    this.menu.open = false;
                    this.menu.shownMessages = -1;
                    if (this.menu.messages) this.menu.messages.length = 0;
                }
                return;
            }

            if (dispatch.ML2 && dispatch.type === 'MESSAGE_DELETE') {
                callDefault(...args);
                setTimeout(() => this.updateMessages(), 0); // because this needs to return for it to continue
                return;
            }

            if (dispatch.type === 'LOAD_MESSAGES_SUCCESS_CACHED') {
                callDefault(...args);
                return this.updateMessages();
            }

            if (dispatch.type !== 'MESSAGE_CREATE' && dispatch.type !== 'MESSAGE_DELETE' && dispatch.type !== 'MESSAGE_DELETE_BULK' && dispatch.type !== 'MESSAGE_UPDATE' && dispatch.type !== 'LOAD_MESSAGES_SUCCESS') return callDefault(...args);

            // console.log('INFO: onDispatchEvent -> dispatch', dispatch);

            if (dispatch.message && dispatch.message.type) return callDefault(...args); // anti other shit 1

            const channel = this.tools.getChannel(dispatch.message ? dispatch.message.channel_id : dispatch.channelId);
            if (!channel) return callDefault(...args);
            const guild = channel.guild_id ? this.tools.getServer(channel.guild_id) : false;

            let author = dispatch.message && dispatch.message.author ? this.tools.getUser(dispatch.message.author.id) : false;
            if (!author) author = ((this.channelMessages[channel.id] || { _map: {} })._map[dispatch.message ? dispatch.message.id : dispatch.id] || {}).author;
            if (!author) {
                // last ditch attempt
                let message = this.getCachedMessage(dispatch.id);
                if (message) author = this.tools.getUser(message.author.id);
            }

            if (!author && !(dispatch.type == 'LOAD_MESSAGES_SUCCESS' || dispatch.type == 'MESSAGE_DELETE_BULK')) return callDefault(...args);

            if (author && author.bot && this.settings.ignoreBots) return callDefault(...args);
            if (author && author.id == this.localUser.id && this.settings.ignoreSelf) return callDefault(...args);
            if (author && this.settings.ignoreBlockedUsers && this.tools.isBlocked(author.id)) return callDefault(...args);
            if (author && author.avatar === 'clyde') return callDefault(...args);

            const isLocalUser = author && author.id === this.localUser.id;

            let guildIsMutedReturn = false;
            let channelIgnoreReturn = false;

            const isInWhitelist = id => this.settings.whitelist.findIndex(m => m === id) != -1;
            const isInBlacklist = id => this.settings.blacklist.findIndex(m => m === id) != -1;
            const guildWhitelisted = guild && isInWhitelist(guild.id);
            const channelWhitelisted = isInWhitelist(channel.id);

            const guildBlacklisted = guild && isInBlacklist(guild.id);
            const channelBlacklisted = isInBlacklist(channel.id);

            if (guild) {
                guildIsMutedReturn = this.settings.ignoreMutedGuilds && this.muteModule.isMuted(guild.id);
                channelIgnoreReturn = (this.settings.ignoreNSFW && channel.nsfw && !channelWhitelisted) || (this.settings.ignoreMutedChannels && (this.muteModule.isChannelMuted(guild.id, channel.id) || (channel.parent_id && this.muteModule.isChannelMuted(channel.parent_id))));
            }

            if (!((this.settings.alwaysLogSelected && this.selectedChannel && this.selectedChannel.id == channel.id) || (this.settings.alwaysLogDM && !guild))) {
                if (guildBlacklisted) {
                    if (!channelWhitelisted) return callDefault(...args); // not whitelisted
                } else if (guildWhitelisted) {
                    if (channelBlacklisted) return callDefault(...args); // channel blacklisted
                    if (channelIgnoreReturn && !channelWhitelisted) return callDefault(...args);
                } else {
                    if (this.settings.onlyLogWhitelist) {
                        if (!channelWhitelisted) return callDefault(...args); // guild not in either list, channel not whitelisted
                    } else {
                        if (channelBlacklisted) return callDefault(...args); // channel blacklisted
                        if (channelIgnoreReturn || guildIsMutedReturn) {
                            if (!channelWhitelisted) return callDefault(...args);
                        }
                    }
                }
            }

            if (dispatch.type == 'LOAD_MESSAGES_SUCCESS') {
                if (!this.settings.restoreDeletedMessages) return callDefault(...args);
                if (dispatch.jump && dispatch.jump.ML2) delete dispatch.jump;
                const deletedMessages = this.deletedMessageRecord[channel.id];
                const purgedMessages = this.purgedMessageRecord[channel.id];
                if ((!deletedMessages && !purgedMessages) || (!this.settings.showPurgedMessages && !this.settings.showDeletedMessages)) return callDefault(...args);
                //console.log('Recursively adding deleted messages');
                /* return callDefault(...args); */
                if (this.settings.showDeletedMessages && deletedMessages) {
                    for (let messageIDX in deletedMessages) {
                        let recordID = deletedMessages[messageIDX];
                        if (!this.messageRecord[recordID]) continue;
                        if (this.messageRecord[recordID].delete_data.hidden) {
                            const mIDX = dispatch.messages.findIndex(m => m.id === recordID);
                            if (mIDX != -1) dispatch.messages.splice(mIDX, 1);
                            continue;
                        }
                        if (this.messageRecord[recordID].message.channel_id != dispatch.channelId || dispatch.messages.findIndex(m => m.id === recordID) != -1) continue;
                        this.antiinfiniteloop = [];
                        this.reAddDeletedMessages(dispatch.messages, recordID, deletedMessages, purgedMessages);
                    }
                }
                if (this.settings.showPurgedMessages && purgedMessages) {
                    for (let messageIDX in purgedMessages) {
                        let recordID = purgedMessages[messageIDX];
                        if (!this.messageRecord[recordID]) continue;
                        if (this.messageRecord[recordID].delete_data.hidden) {
                            const mIDX = dispatch.messages.findIndex(m => m.id === recordID);
                            if (mIDX != -1) dispatch.messages.splice(mIDX, 1);
                            continue;
                        }
                        if (this.messageRecord[recordID].message.channel_id != dispatch.channelId || dispatch.messages.findIndex(m => m.id == recordID) != -1) continue;
                        this.antiinfiniteloop = [];
                        this.reAddDeletedMessages(dispatch.messages, recordID, deletedMessages, purgedMessages);
                    }
                }
                callDefault(...args);
                this.updateMessages();
                return;
            }

            if (dispatch.type == 'MESSAGE_DELETE') {
                const deleted = this.getCachedMessage(dispatch.id, dispatch.channelId);

                if (this.deletedMessageRecord[channel.id] && this.deletedMessageRecord[channel.id].findIndex(m => m === deleted.id) != -1) {
                    if (!this.settings.showDeletedMessages) callDefault(...args);
                    return;
                }

                if (this.settings.showDeletedCount && !deleted.type) {
                    if (!this.deletedChatMessagesCount[channel.id]) this.deletedChatMessagesCount[channel.id] = 0;
                    if (!this.selectedChannel || this.selectedChannel.id != channel.id) this.deletedChatMessagesCount[channel.id]++;
                }

                if (this.settings.aggresiveMessageCaching) {
                    const channelMessages = this.channelMessages[channel.id];
                    if (!channelMessages || !channelMessages.ready) this.cacheChannelMessages(channel.id);
                }

                if (!deleted) return callDefault(...args); // nothing we can do past this point..

                if (deleted.type) return callDefault(...args);
                if (guild ? this.settings.toastToggles.deleted && ((isLocalUser && !this.settings.toastToggles.disableToastsForLocal) || !isLocalUser) : this.settings.toastTogglesDMs.deleted && !isLocalUser) {
                    this.showToast(`Message deleted from ${this.getLiteralName(channel.guild_id, channel.id)}`, {
                        type: 'error',
                        onClick: () => this.openWindow('deleted'),
                        onContext: () => this.jumpToMessage(dispatch.channelId, dispatch.id, guild && guild.id),
                        timeout: 4500
                    });
                }

                if ((!this.selectedChannel || this.selectedChannel.id != channel.id) && (guild ? this.settings.toastToggles.ghostPings : this.settings.toastTogglesDMs.ghostPings) && this.tools.isMentioned(deleted, this.localUser.id)) {
                    this.showToast(`You got ghost pinged in ${this.getLiteralName(channel.guild_id, channel.id)}`, {
                        type: 'warning',
                        onClick: () => this.openWindow('ghostpings'),
                        onContext: () => this.jumpToMessage(dispatch.channelId, dispatch.id, guild && guild.id),
                        timeout: 4500
                    });
                }

                this.saveDeletedMessage(deleted, this.deletedMessageRecord);
                // if (this.settings.cacheAllImages) this.cacheImages(deleted);
                if (!this.settings.showDeletedMessages) callDefault(...args);
                this.saveData();
            } else if (dispatch.type == 'MESSAGE_DELETE_BULK') {
                if (this.settings.showDeletedCount) {
                    if (!this.deletedChatMessagesCount[channel.id]) this.deletedChatMessagesCount[channel.id] = 0;
                    if (!this.selectedChannel || this.selectedChannel.id != channel.id) this.deletedChatMessagesCount[channel.id] += dispatch.ids.length;
                }

                let failedMessage = false;

                for (let i = 0; i < dispatch.ids.length; i++) {
                    const purged = this.getCachedMessage(dispatch.ids[i], channel.id);
                    if (!purged) {
                        failedMessage = true;
                        continue;
                    }
                    this.saveDeletedMessage(purged, this.purgedMessageRecord);
                }

                if (failedMessage && this.aggresiveMessageCaching)
                    // forcefully cache the channel in case there are active convos there
                    this.cacheChannelMessages(channel.id);
                else if (this.settings.aggresiveMessageCaching) {
                    const channelMessages = this.channelMessages[channel.id];
                    if (!channelMessages || !channelMessages.ready) this.cacheChannelMessages(channel.id);
                }

                if (guild ? this.settings.toastToggles.deleted : this.settings.toastTogglesDMs.deleted) {
                    this.showToast(`${dispatch.ids.length} messages bulk deleted from ${this.getLiteralName(channel.guild_id, channel.id)}`, {
                        type: 'error',
                        onClick: () => this.openWindow('purged'),
                        onContext: () => this.jumpToMessage(channel.id, undefined, guild && guild.id),
                        timeout: 4500
                    });
                }

                if (!this.settings.showPurgedMessages) callDefault(...args);
                this.saveData();
            } else if (dispatch.type == 'MESSAGE_UPDATE') {
                if (!dispatch.message.edited_timestamp) {
                    let last = this.getCachedMessage(dispatch.message.id);
                    if (last) last.embeds = dispatch.message.embeds;
                    return callDefault(...args);
                }

                if (this.settings.showEditedCount) {
                    if (!this.editedChatMessagesCount[channel.id]) this.editedChatMessagesCount[channel.id] = 0;
                    if (!this.selectedChannel || this.selectedChannel.id != channel.id) this.editedChatMessagesCount[channel.id]++;
                }

                if (this.settings.aggresiveMessageCaching) {
                    const channelMessages = this.channelMessages[channel.id];
                    if (!channelMessages || !channelMessages.ready) this.cacheChannelMessages(channel.id);
                }

                const last = this.getCachedMessage(dispatch.message.id, channel.id);
                const lastEditedSaved = this.getEditedMessage(dispatch.message.id, channel.id);

                // if we have lastEdited then we can still continue as we have all the data we need to process it.
                if (!last && !lastEditedSaved) return callDefault(...args); // nothing we can do past this point..

                if (lastEditedSaved) {
                    // last is not needed, we have all the data already saved
                    // console.log(lastEditedSaved.message);
                    // console.log(dispatch.message);
                    if (lastEditedSaved.message.content === dispatch.message.content) {
                        if (!this.settings.showEditedMessages) callDefault(...args);
                        return; // we don't care about that
                    }
                    lastEditedSaved.edit_history.push({
                        content: lastEditedSaved.message.content,
                        time: new Date().getTime()
                    });
                    lastEditedSaved.message.content = dispatch.message.content;
                } else {
                    if (last.content === dispatch.message.content) {
                        if (!this.settings.showEditedMessages) callDefault(...args);
                        return; // we don't care about that
                    }
                    let data = this.createMiniFormattedData(last);
                    data.edit_history = [
                        {
                            content: last.content,
                            time: new Date().getTime()
                        }
                    ];
                    data.message.content = dispatch.message.content;
                    this.messageRecord[data.message.id] = data;
                    if (!this.editedMessageRecord[channel.id]) this.editedMessageRecord[channel.id] = [];
                    this.editedMessageRecord[channel.id].push(data.message.id);
                }

                if (guild ? this.settings.toastToggles.edited && ((isLocalUser && !this.settings.toastToggles.disableToastsForLocal) || !isLocalUser) : this.settings.toastTogglesDMs.edited && !isLocalUser) {
                    this.showToast(`Message edited in ${this.getLiteralName(channel.guild_id, channel.id)}`, {
                        type: 'info',
                        onClick: () => this.openWindow('edited'),
                        onContext: () => this.jumpToMessage(channel.id, dispatch.message.id, guild && guild.id),
                        timeout: 4500
                    });
                }

                if ((!this.selectedChannel || this.selectedChannel.id != channel.id) && (guild ? this.settings.toastToggles.ghostPings : this.settings.toastTogglesDMs.ghostPings) && lastEditedSaved && lastEditedSaved.local_mentioned && !this.tools.isMentioned(dispatch.message, this.localUser.id)) {
                    this.showToast(`You got ghost pinged in ${this.getLiteralName(channel.guild_id, channel.id)}`, {
                        type: 'warning',
                        onClick: () => this.openWindow('ghostpings'),
                        onContext: () => this.jumpToMessage(dispatch.channelId, dispatch.id, guild && guild.id),
                        timeout: 4500
                    });
                }

                if (!this.settings.showEditedMessages) callDefault(...args);
                else {
                    const msg = this.cachedMessageRecord.find(m => m.id == dispatch.message.id);
                    if (msg) msg.content = dispatch.message.content;
                    const cachedChannel = this.channelMessages[dispatch.message.channel_id];
                    if (cachedChannel && cachedChannel.ready) {
                        const cachedMessage = cachedChannel._map[dispatch.message.id];
                        // wtf?
                        if (cachedMessage) {
                            cachedMessage.content = dispatch.message.content; // probably bad idea but it works
                            cachedMessage.contentParsed = this.tools.parse(dispatch.message.content, true, { channelId: dispatch.message.channel_id }); // omega ghetto
                            cachedMessage.editedTimestamp = this.tools.createMomentObject(dispatch.message.edited_timestamp); // don't ask how I found this
                        }
                    }
                }
                this.saveData();
            } else if (dispatch.type == 'MESSAGE_CREATE' && dispatch.message && (dispatch.message.content.length || (dispatch.attachments && dispatch.attachments.length) || (dispatch.embeds && dispatch.embeds.length)) && dispatch.message.state != 'SENDING' && !dispatch.optimistic && !dispatch.message.type) {
                if (this.cachedMessageRecord.findIndex(m => m.id === dispatch.message.id) != -1) return callDefault(...args);
                this.cachedMessageRecord.push(dispatch.message);

                /* if (this.menu.open && this.menu.selectedTab == 'sent') this.refilterMessages(); */

                if (this.settings.aggresiveMessageCaching) {
                    const channelMessages = this.channelMessages[channel.id];
                    if (!channelMessages || !channelMessages.ready) this.cacheChannelMessages(channel.id);
                }
                if ((guild ? this.settings.toastToggles.sent : this.settings.toastTogglesDMs.sent) && (!this.selectedChannel || this.selectedChannel.id != channel.id)) {
                    this.showToast(`Message sent in ${this.getLiteralName(channel.guild_id, channel.id)}`, { type: 'info', onClick: () => this.openWindow('sent'), onContext: () => this.jumpToMessage(channel.id, dispatch.message.id, guild && guild.id), timeout: 4500 });
                }

                callDefault(...args);
            } else callDefault(...args);
        } catch (err) {
            ZLibrary.Logger.stacktrace(this.getName(), 'Error in onDispatchEvent', err);
        }
    }
    updateMessages() {
        if (!this.settings.showEditedMessages && !this.settings.showDeletedMessages && !this.settings.showPurgedMessages) return;
        if (!this.selectedChannel) return;

        const chat = document.querySelector('.' + this.classes.chat);
        if (!chat) return;
        const messages = chat.getElementsByClassName(this.classes.messages);

        const onClickEditedTag = e => {
            this.menu.filter = `message:${e.target.messageId}`;
            this.openWindow('edited');
        };

        for (let i = 0; i < messages.length; i++) {
            try {
                const message = messages[i];
                const props = ZLibrary.ReactTools.getOwnerInstance(message).props;
                const msg = props.message || (props.style.borderRadius == 2 && props.children && props.children._owner && props.children._owner.memoizedProps.message);
                if (!msg) continue;
                const mid = msg.id;
                const channID = msg.channel_id;
                let markup = message.getElementsByClassName(this.classes.markup)[0];
                if (!markup) continue; // gay
                let record;
                let wasDeleted = false;
                if (message.className.indexOf(this.style.deleted) === -1 && ((this.deletedMessageRecord[channID] && this.deletedMessageRecord[channID].findIndex(m => m === mid) !== -1) || (this.purgedMessageRecord[channID] && this.purgedMessageRecord[channID].findIndex(m => m === mid) != -1))) {
                    record = this.getSavedMessage(mid);
                    if (!record) continue;
                    const edits = message.getElementsByClassName(this.style.edited);
                    for (let i = 0; i < edits.length; i++) if (edits[i].tooltip) edits[i].tooltip.disabled = true;
                    message.classList.add(this.style.deleted);
                    new ZeresPluginLibrary.EmulatedTooltip(markup.textContent ? markup : message.getElementsByClassName(this.classes.content)[0].lastChild, 'Deleted at ' + this.createTimeStamp(record.delete_data.time), { side: 'left' });
                    wasDeleted = true;
                }
                if (!this.settings.showEditedMessages) continue;
                record = this.getEditedMessage(mid, channID);
                if (record && !record.edits_hidden) {
                    if (markup.edits === record.edit_history.length) continue;
                    markup.edits = record.edit_history.length; // todo, randomize
                    markup.classList.add(this.style.edited);
                    /* HACK */
                    if (!markup.fucked) {
                        const original = markup.cloneNode(true);
                        const parent = markup.parentElement;
                        markup.remove();
                        parent.appendChild(original);
                        markup = original;
                        markup.fucked = true;
                    }
                    /* HACK */
                    const headerWidth = markup.className.indexOf(this.classes.isCompact) !== -1 && markup.firstChild && markup.firstChild.getBoundingClientRect ? markup.firstChild.getBoundingClientRect().width : 0;
                    let backup;
                    if (headerWidth) backup = markup.firstChild.cloneNode(true);
                    while (markup.firstChild) markup.removeChild(markup.firstChild);
                    let e = 0;
                    let max = record.edit_history.length;
                    if (this.settings.maxShownEdits) {
                        if (record.edit_history.length > this.settings.maxShownEdits) {
                            if (this.settings.hideNewerEditsFirst) {
                                max = this.settings.maxShownEdits;
                            } else {
                                e = record.edit_history.length - this.settings.maxShownEdits;
                            }
                        }
                    }
                    for (; e < max; e++) {
                        const editedMarkup = this.formatMarkup(record.edit_history[e].content, record.message.channel_id);
                        const editedTag = document.createElement('time');
                        editedTag.className = this.multiClasses.edited;
                        editedTag.innerText = '(edited)';
                        editedTag.messageId = mid;
                        editedTag.addEventListener('click', onClickEditedTag);
                        editedMarkup.firstElementChild.appendChild(editedTag);
                        editedMarkup.classList.add(this.style.edited);
                        editedMarkup.firstChild.classList.remove(this.classes.markup);
                        editedMarkup.firstElementChild.editNum = e;
                        const time = record.edit_history[e].time; // compatibility
                        if (!wasDeleted) editedMarkup.tooltip = new ZeresPluginLibrary.EmulatedTooltip(editedMarkup, 'Edited at ' + (typeof time === 'string' ? time : this.createTimeStamp(time)), { side: 'left' });
                        if (backup) {
                            editedMarkup.insertBefore(backup, editedMarkup.firstChild);
                            backup = undefined;
                        } else if (headerWidth) editedMarkup.style.marginLeft = headerWidth + 5 + 'px';
                        markup.appendChild(editedMarkup);
                    }
                    const editedMarkup = this.formatMarkup(record.message.content, record.message.channel_id);
                    editedMarkup.firstChild.classList.remove(this.classes.markup);
                    if (headerWidth) editedMarkup.style.marginLeft = headerWidth + 5 + 'px';
                    markup.appendChild(editedMarkup);
                } else if (record && record.edit_history && record.edits_hidden && !markup.touched) {
                    const editedText = markup.getElementsByClassName(this.multiClasses.edited.split(/ /g)[0])[0];
                    if (!editedText) continue; // dunno
                    editedText.messageId = mid;
                    editedText.addEventListener('click', onClickEditedTag);
                    markup.touched = true;
                }
            } catch (e) {
                ZLibrary.Logger.stacktrace(this.getName(), 'Error in updateMessages', e);
            }
        }
    }
    /* ==================================================-|| START MENU ||-================================================== */
    processUserRequestQueue() {
        if (!this.processUserRequestQueue.queueIntervalTime) this.processUserRequestQueue.queueIntervalTime = 500;
        if (this.menu.queueInterval) return;
        const messageDataManager = () => {
            if (!this.menu.userRequestQueue.length) {
                clearInterval(this.menu.queueInterval);
                this.menu.queueInterval = 0;
                return;
            }
            const data = this.menu.userRequestQueue.shift();
            this.tools
                .getUserAsync(data.id)
                .then(res => {
                    for (let ss of data.success) ss(res);
                })
                .catch(reason => {
                    if (reason.status == 429 && typeof reason.body.retry_after === 'number') {
                        clearInterval(this.menu.queueInterval);
                        this.menu.queueInterval = 0;
                        this.processUserRequestQueue.queueIntervalTime += 50;
                        setTimeout(messageDataManager, reason.body.retry_after);
                        ZLibrary.Logger.warn(this.getName(), 'Rate limited, retrying in', reason.body.retry_after, 'ms');
                        this.menu.userRequestQueue.push(data);
                        return;
                    }
                    ZLibrary.Logger.warn(this.getName(), `Failed to get info for ${data.username}, reason:`, reason);
                    for (let ff of data.fail) ff();
                });
        };
        this.menu.queueInterval = setInterval(() => messageDataManager(), this.processUserRequestQueue.queueIntervalTime);
    }
    patchModal() {
        // REQUIRED
        const onChangeOrder = el => {
            this.settings.reverseOrder = !this.settings.reverseOrder;
            el.target.innerText = 'Sort direction: ' + (!this.settings.reverseOrder ? 'new - old' : 'old - new'); // maybe a func?
            this.saveSettings();
            this.refilterMessages();
        };

        const onClearLog = e => {
            e.preventDefault();
            if (document.getElementById('ml2-filter').parentElement.parentElement.className.indexOf(this.createTextBox.classes.focused[0]) != -1) return;
            this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop;
            let type = this.menu.selectedTab;
            if (type === 'ghostpings') type = 'ghost pings';
            else {
                type += ' messages';
            }
            ZLibrary.Modals.showConfirmationModal('Clear log', `Are you sure you want to delete all ${type}${this.menu.filter.length ? ' that also match filter' : ''}?`, {
                confirmText: 'Confirm',
                onConfirm: () => {
                    if (this.menu.selectedTab == 'sent') {
                        if (!this.menu.filter.length) for (let id of this.menu.messages) this.cachedMessageRecord.splice(this.cachedMessageRecord.findIndex(m => m.id === id), 1);
                        else this.cachedMessageRecord.length = 0; // hack, does it cause a memory leak?
                    } else {
                        for (let id of this.menu.messages) this.deleteMessageFromRecords(id);
                        this.saveData();
                    }
                    this.menu.refilterOnMount = true;
                }
            });
        };

        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.DiscordModules.ConfirmationModal.prototype, 'handleClose', (thisObj, args, original) => {
                if (thisObj.props.ml2Data) onChangeOrder(args[0]);
                else original(...args);
            })
        );

        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.DiscordModules.ConfirmationModal.prototype, 'handleSubmit', (thisObj, args, original) => {
                if (thisObj.props.ml2Data) onClearLog(args[0]);
                else original(...args);
            })
        );

        this.unpatches.push(
            ZLibrary.Patcher.instead(this.getName(), ZLibrary.DiscordModules.ConfirmationModal.prototype, 'componentDidMount', (thisObj, args, original) => {
                if (thisObj.props.ml2Data) {
                    if (this.menu.refilterOnMount) {
                        this.refilterMessages();
                        this.menu.refilterOnMount = false;
                    }
                    document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop = this.scrollPosition;
                }
                return original(...args);
            })
        );
    }
    // >>-|| POPULATION ||-<<
    createMessageGroup(message) {
        let deleted = false;
        let edited = false;
        let details = 'Sent in';
        let channel = this.tools.getChannel(message.channel_id);
        let timestamp = message.timestamp;
        let author = this.tools.getUser(message.author.id);
        let noUserInfo = false;
        let userInfoBeingRequested = true;
        const isBot = message.author.bot;
        const record = this.messageRecord[message.id];
        if (record) {
            deleted = !!record.delete_data;
            edited = !!record.edit_history;

            if (deleted && edited) {
                details = 'Edited and deleted from';
                timestamp = record.delete_data.time;
            } else if (deleted) {
                details = 'Deleted from';
                timestamp = record.delete_data.time;
            } else if (edited) {
                details = 'Last edit in'; // todo: purged?
                if (typeof record.edit_history[record.edit_history.length - 1].time !== 'string') timestamp = record.edit_history[record.edit_history.length - 1].time;
            }
        }

        details += ` ${this.getLiteralName(message.guild_id || (channel && channel.guild_id), message.channel_id)} `;

        details += `at ${this.createTimeStamp(timestamp, true)}`;

        const classes = this.createMessageGroup.classes;
        const getAvatarOf = user => {
            if (!user.avatar) return '/assets/322c936a8c8be1b803cd94861bdfa868.png';
            return `https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png?size=128`;
        };

        // minify?
        // what is wrapper-3t9DeA and how do I get it
        // wtf is avatar-VxgULZ
        const element = this.parseHTML(`<div class="${classes.containerBounded}" style="padding-bottom: 25px; padding-top: 0px">
                                <div class="${classes.message}">
                                    <div class="${classes.header}">
                                        <div class="${classes.avatar}">
                                            <div class="wrapper-3t9DeA da-wrapper" role="img" style="width: 40px; height: 40px;">
                                                <img src="${getAvatarOf(message.author)}" class="${classes.avatarImg}" aria-hidden="true" style="border-radius: 100%;">
                                            </div>
                                        </div>
                                        <h2 class="${classes.headerMeta}">
                                            <span class="">
                                                <span tabindex="0" class="${classes.username}" role="button">
                                                ${message.author.username}
                                                </span>
                                                ${(isBot &&
                                                    `<span class="${classes.botTag}">
                                                    BOT
                                                </span>`) ||
                                                    ''}
                                            </span>
                                            <div class="${classes.timestamp}" style="display:inline">
                                                ${details}
                                            </div>
                                        </h2>
                                    </div>
                                    <div class="${classes.content}">
                                    </div>
                                </div>
                            </div>`);
        const profImg = element.getElementsByClassName(classes.avatarImgSingle)[0];
        profImg.onerror = () => {
            profImg.src = '/assets/322c936a8c8be1b803cd94861bdfa868.png';
        };
        const verifyProfilePicture = () => {
            if (message.author.avatar != author.avatar && author.avatar) {
                profImg.src = getAvatarOf(author);
                if (record) {
                    record.message.author.avatar = author.avatar;
                }
            } else {
                if (record) record.message.author.avatar = null;
            }
        };
        if (!isBot) {
            if (!author) {
                author = message.author;
                if (this.menu.userRequestQueue.findIndex(m => m.id === author.id) == -1) {
                    this.menu.userRequestQueue.push({
                        id: author.id,
                        username: author.username,
                        success: [
                            res => {
                                author = $.extend(true, {}, res);
                                verifyProfilePicture();
                                userInfoBeingRequested = false;
                            }
                        ],
                        fail: [
                            () => {
                                noUserInfo = true;
                                userInfoBeingRequested = false;
                            }
                        ]
                    });
                } else {
                    const dt = this.menu.userRequestQueue.find(m => m.id === author.id);
                    dt.success.push(res => {
                        author = $.extend(true, {}, res);
                        verifyProfilePicture();
                        userInfoBeingRequested = false;
                    });
                    dt.fail.push(() => {
                        noUserInfo = true;
                        userInfoBeingRequested = false;
                    });
                }
            } else {
                userInfoBeingRequested = false;
                verifyProfilePicture();
            }
        }
        const profIcon = element.getElementsByClassName(classes.avatarSingle)[0];
        profIcon.addEventListener('click', () => {
            if (isBot) return this.showToast('User is a bot, this action is not possible on a bot.', { type: 'error', timeout: 5000 });
            if (userInfoBeingRequested) return this.showToast('Please wait, user profile is being requested', { type: 'info', timeout: 5000 });
            if (noUserInfo) return this.showToast('Could not get user info!', { type: 'error' });
            this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop;
            ZLibrary.Popouts.showUserPopout(profIcon, author);
        });
        profIcon.addEventListener('contextmenu', e => {
            if (isBot) return this.showToast('User is a bot, this action is not possible on a bot.', { type: 'error', timeout: 5000 });
            if (userInfoBeingRequested) return this.showToast('Please wait, user profile is being requested', { type: 'info', timeout: 5000 });
            if (noUserInfo) return this.showToast('Could not get user info! You can only delete or copy to clipboard!', { timeout: 5000 });
            ZLibrary.WebpackModules.getByProps('openUserContextMenu').openUserContextMenu(e, author, channel || this.menu.randomValidChannel);
        });
        const nameLink = element.getElementsByClassName(ZLibrary.DiscordClasses.Messages.username.value.split(/ /g)[0])[0];
        nameLink.addEventListener('click', () => {
            if (isBot) return this.showToast('User is a bot, this action is not possible on a bot.', { type: 'error', timeout: 5000 });
            if (userInfoBeingRequested) return this.showToast('Please wait, user profile is being requested', { type: 'info', timeout: 5000 });
            if (noUserInfo) return this.showToast('Could not get user info!', { type: 'error' });
            this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop; // todo: turn this into a function
            ZLibrary.Popouts.showUserPopout(nameLink, author);
        });
        nameLink.addEventListener('contextmenu', e => {
            if (isBot) return this.showToast('User is a bot, this action is not possible on a bot.', { type: 'error', timeout: 5000 });
            if (userInfoBeingRequested) return this.showToast('Please wait, user profile is being requested', { type: 'info', timeout: 5000 });
            if (noUserInfo) return this.showToast('Could not get user info! You can only delete or copy to clipboard!', { type: 'error', timeout: 5000 });
            ZLibrary.WebpackModules.getByProps('openUserContextMenu').openUserContextMenu(e, author, channel || this.menu.randomValidChannel);
        });
        const timestampEl = element.getElementsByClassName(classes.timestampSingle)[0];
        timestampEl.addEventListener('contextmenu', e => {
            const messages = element.querySelectorAll(`div[class] div[class] div .${classes.markupSingle}`);
            if (!messages.length) return;
            const messageIds = [];
            for (let i = 0; i < messages.length; i++) if (messages[i] && messages[i].messageId) messageIds.push(messages[i].messageId);
            if (!messageIds.length) return;
            const menu = new ZLibrary.ContextMenu.Menu();
            const remove = new ZLibrary.ContextMenu.TextItem('Remove group from log');
            const copyMessage = new ZLibrary.ContextMenu.TextItem('Copy formatted messages');
            remove.element.addEventListener('click', () => {
                menu.removeMenu();
                let invalidatedChannelCache = false;
                for (let msgid of messageIds) {
                    const record = this.messageRecord[msgid];
                    if (!record) continue; // the hell
                    if ((record.edit_history && !record.edits_hidden) || (record.delete_data && !record.delete_data.hidden)) this.invalidateChannelCache((invalidatedChannelCache = record.message.channel_id));
                    this.deleteMessageFromRecords(msgid);
                }
                if (invalidatedChannelCache) this.cacheChannelMessages(invalidatedChannelCache);
                this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                this.saveData();
            });
            copyMessage.element.addEventListener('click', () => {
                let result = '';
                for (let msgid of messageIds) {
                    const record = this.messageRecord[msgid];
                    if (!record) continue;
                    if (!result.length) result += `> **${record.message.author.username}** | ${this.createTimeStamp(record.message.timestamp, true)}\n`;
                    result += `> ${record.message.content.replace(/\n/g, '\n> ')}\n`;
                }
                this.nodeModules.electron.clipboard.writeText(result);
                this.showToast('Copied!', { type: 'success' });
                menu.removeMenu();
            });
            menu.addItems(copyMessage, remove);
            menu.show(e.clientX, e.clientY);
        });
        timestampEl.addEventListener('click', e => {
            if (!this.menu.deleteKeyDown) return;
            const messages = element.querySelectorAll(`div[class] div[class] div .${classes.markupSingle}`);
            if (!messages.length) return;
            const messageIds = [];
            for (let i = 0; i < messages.length; i++) if (messages[i] && messages[i].messageId) messageIds.push(messages[i].messageId);
            if (!messageIds.length) return;
            let invalidatedChannelCache = false;
            for (let msgid of messageIds) {
                const record = this.messageRecord[msgid];
                if (!record) continue; // the hell
                if ((record.edit_history && !record.edits_hidden) || (record.delete_data && !record.delete_data.hidden)) this.invalidateChannelCache((invalidatedChannelCache = record.message.channel_id));
                this.deleteMessageFromRecords(msgid);
            }
            if (invalidatedChannelCache) this.cacheChannelMessages(invalidatedChannelCache);
            this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
            this.saveData();
        });
        const messageContext = e => {
            let target = e.target;
            if ((!target.classList.contains(this.classes.containerBounded) && !target.classList.contains('mention')) || (target.tagName == 'DIV' && target.classList.contains(ZLibrary.WebpackModules.getByProps('imageError').imageError.split(/ /g)[0]))) {
                let isMarkup = false;
                let isEdited = false;
                let isBadImage = target.tagName == 'DIV' && target.classList == ZLibrary.WebpackModules.getByProps('imageError').imageError;
                if (!isBadImage) {
                    while (!target.classList.contains(this.classes.containerBounded) && !(isMarkup = target.classList.contains(this.classes.markup))) {
                        if (target.classList.contains(this.style.edited)) isEdited = target;
                        target = target.parentElement;
                    }
                }

                if (isMarkup || isBadImage) {
                    const messageId = target.messageId;
                    if (!messageId) return;
                    const record = this.getSavedMessage(messageId);
                    if (!record) return;
                    let editNum = -1;
                    if (isEdited) editNum = isEdited.edit;
                    const menu = new ZLibrary.ContextMenu.Menu();
                    if (channel) {
                        let jump = new ZLibrary.ContextMenu.TextItem('Jump to message');
                        jump.element.addEventListener('click', () => {
                            this.jumpToMessage(message.channel_id, messageId, message.guild_id);
                            menu.removeMenu();
                            return;
                        });
                        menu.addItems(jump);
                    }
                    let remove = new ZLibrary.ContextMenu.TextItem('Remove from log');
                    let copyId = new ZLibrary.ContextMenu.TextItem('Copy message ID');
                    remove.element.addEventListener('click', () => {
                        let invalidatedChannelCache = false;
                        if ((record.edit_history && !record.edits_hidden) || (record.delete_data && !record.delete_data.hidden)) this.invalidateChannelCache((invalidatedChannelCache = record.message.channel_id));
                        this.deleteMessageFromRecords(messageId);
                        this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                        menu.removeMenu();
                        if (invalidatedChannelCache) this.cacheChannelMessages(invalidatedChannelCache);
                        this.saveData();
                    });
                    if (!isBadImage || record.message.content.length) {
                        let copyText = new ZLibrary.ContextMenu.TextItem('Copy text');
                        let copyMessage = new ZLibrary.ContextMenu.TextItem('Copy formatted message');
                        copyText.element.addEventListener('click', () => {
                            this.nodeModules.electron.clipboard.writeText(editNum != -1 ? record.edit_history[editNum].content : record.message.content);
                            this.showToast('Copied!', { type: 'success' });
                            menu.removeMenu();
                        });
                        copyMessage.element.addEventListener('click', () => {
                            const content = editNum != -1 ? record.edit_history[editNum].content : record.message.content;
                            const result = `> **${record.message.author.username}** | ${this.createTimeStamp(record.message.timestamp, true)}\n> ${content.replace(/\n/g, '\n> ')}`;
                            this.nodeModules.electron.clipboard.writeText(result);
                            this.showToast('Copied!', { type: 'success' });
                            menu.removeMenu();
                        });
                        menu.addItems(copyText, copyMessage);
                    }
                    copyId.element.addEventListener('click', () => {
                        this.nodeModules.electron.clipboard.writeText(messageId); // todo: store electron or writeText somewhere?
                        this.showToast('Copied!', { type: 'success' });
                        menu.removeMenu();
                    });
                    if (record.delete_data && record.delete_data.hidden) {
                        let unhideDelete = new ZLibrary.ContextMenu.TextItem('Unhide deleted message');
                        unhideDelete.element.addEventListener('click', () => {
                            record.delete_data.hidden = false;
                            this.invalidateChannelCache(record.message.channel_id); // good idea?
                            this.cacheChannelMessages(record.message.channel_id);
                            this.saveData();
                            this.showToast('Unhidden!', { type: 'success' });
                            menu.removeMenu();
                        });
                        menu.addItems(unhideDelete);
                    }
                    if (record.edit_history) {
                        if (editNum != -1) {
                            let deleteEdit = new ZLibrary.ContextMenu.TextItem('Delete edit');
                            deleteEdit.element.addEventListener('click', () => {
                                this.deleteEditedMessageFromRecord(messageId, editNum);
                                this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                                this.showToast('Deleted!', { type: 'success' });
                                menu.removeMenu();
                            });
                            menu.addItems(deleteEdit);
                        }
                        if (record.edits_hidden) {
                            let unhideEdits = new ZLibrary.ContextMenu.TextItem('Unhide edits');
                            unhideEdits.element.addEventListener('click', () => {
                                record.edits_hidden = false;
                                this.saveData();
                                this.showToast('Unhidden!', { type: 'success' });
                                menu.removeMenu();
                            });
                            menu.addItems(unhideEdits);
                        }
                    }
                    menu.addItems(remove, copyId);
                    menu.show(e.clientX, e.clientY);
                    return;
                }
            }
        };
        element.getElementsByClassName(classes.messageSingle)[0].addEventListener('contextmenu', e => messageContext(e));
        element.getElementsByClassName(classes.messageSingle)[0].addEventListener('click', e => {
            if (!this.menu.deleteKeyDown) return;
            let target = e.target;
            if (!target.classList.contains(this.classes.containerBounded)) {
                let isMarkup = false;
                let isEdited = false;
                let isBadImage = target.tagName == 'DIV' && target.classList == ZLibrary.WebpackModules.getByProps('imageError').imageError;
                if (!isBadImage) {
                    while (!target.classList.contains(this.classes.containerBounded) && !(isMarkup = target.classList.contains(this.classes.markup))) {
                        if (target.classList.contains(this.style.edited)) isEdited = target;
                        target = target.parentElement;
                    }
                }
                if (!isMarkup && !isBadImage) return;
                const messageId = target.messageId;
                if (!messageId) return;
                const record = this.messageRecord[messageId];
                if (!record) return;
                this.invalidateChannelCache(record.message.channel_id); // good idea?
                this.cacheChannelMessages(record.message.channel_id);
                if (isEdited) {
                    this.deleteEditedMessageFromRecord(messageId, isEdited.edit);
                } else {
                    this.deleteMessageFromRecords(messageId);
                }
                this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                this.saveData();
            }
        });
        return element;
    }
    populateParent(parent, messages) {
        let lastMessage;
        let lastType; /* unused */
        let messageGroup;
        const registerMentions = markup => {
            const mentions = markup.getElementsByClassName('mention');
            for (let mention of mentions) {
                mention.addEventListener('click', () => {
                    this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop;
                });
            }
        };
        const populate = i => {
            try {
                // todo: maybe make the text red if it's deleted?
                const messageId = messages[i];
                const record = this.getSavedMessage(messageId);
                const message = record ? record.message : this.getMessageAny(messageId);
                if (!message) return;
                // todo: get type and use it
                if (!messageGroup /*  || !lastType */ || !lastMessage || lastMessage.channel_id != message.channel_id || lastMessage.author.id != message.author.id || new Date(message.timestamp).getDate() !== new Date(lastMessage.timestamp).getDate() || (message.attachments.length && message.content.length)) {
                    messageGroup = this.createMessageGroup(message);
                }
                lastMessage = message;
                const messageContent = messageGroup.getElementsByClassName(this.multiClasses.message.cozy.content.split(/ /g)[0])[0];
                const div = document.createElement('div');
                const divParent = document.createElement('div');
                div.className = this.multiClasses.markup;
                div.messageId = messageId;
                if (record && record.edit_history) {
                    div.classList.add(this.style.edited);
                    for (let ii = 0; ii < record.edit_history.length; ii++) {
                        const hist = record.edit_history[ii];
                        const editedMarkup = this.formatMarkup(hist.content, message.channel_id).firstChild;
                        editedMarkup.insertAdjacentHTML('beforeend', `<time class="${this.multiClasses.edited}">(edited)</time>`); // TODO, change this
                        editedMarkup.tooltip = new ZeresPluginLibrary.EmulatedTooltip(editedMarkup, 'Edited at ' + (typeof hist.time === 'string' ? hist.time : this.createTimeStamp(hist.time)), { side: 'left' });
                        editedMarkup.classList.add(this.style.edited);
                        editedMarkup.edit = ii;
                        registerMentions(editedMarkup);
                        div.appendChild(editedMarkup);
                    }
                }
                const contentMarkup = this.formatMarkup(message.content, message.channel_id).firstChild;
                contentMarkup.messageId = message.id;
                registerMentions(contentMarkup);
                div.appendChild(contentMarkup);
                if (!record) {
                    const channel = this.tools.getChannel(message.channel_id);
                    const guild = this.tools.getServer(channel && channel.guild_id);
                    divParent.addEventListener('click', () => this.jumpToMessage(message.channel_id, message.id, guild && guild.id));
                }
                divParent.appendChild(div);
                // todo, embeds
                // how do I do embeds?

                // why don't attachments show for sent messages? what's up with that?
                if (message.attachments.length) {
                    // const attachmentsContent = this.parseHTML(`<div class="${this.multiClasses.message.cozy.content}"></div>`);
                    const attemptToUseCachedImage = (attachmentId, attachmentIdx, hidden) => {
                        const cachedImage = this.imageCacheRecord[attachmentId];
                        if (!cachedImage) return false;
                        const img = document.createElement('img');
                        img.classList = ZLibrary.WebpackModules.getByProps('clickable').clickable;
                        /* img.classList = ZLibrary.WebpackModules.getByProps('clickable').clickable; */
                        img.messageId = messageId;
                        img.idx = attachmentIdx;
                        img.id = attachmentId; // USED FOR FINDING THE IMAGE THRU CONTEXT MENUS
                        if (hidden) {
                            img.src = `https://i.clouds.tf/q2vy/r8q6.png#${record.message.channel_id},${img.id}`;
                            img.width = 200;
                        } else {
                            img.src = cachedImage.thumbnail; // has channel and attachment id embedded
                            img.width = 256;
                        }
                        img.addEventListener('click', e => {
                            if (!this.menu.deleteKeyDown) return this.nodeModules.electron.shell.openExternal(`file://${this.imageCacheDir}/${attachmentId}${cachedImage.extension}`);
                            this.deleteMessageFromRecords(messageId);
                            this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                            this.saveData();
                        });
                        divParent.appendChild(img);
                        return true;
                    };
                    const handleCreateImage = (attachment, idx) => {
                        if (attachment.url == 'ERROR') {
                            if (attemptToUseCachedImage(attachment.id, idx, attachment.hidden)) return;
                            const imageErrorDiv = document.createElement('div');
                            imageErrorDiv.classList = ZLibrary.WebpackModules.getByProps('imageError').imageError;
                            imageErrorDiv.messageId = messageId;
                            divParent.appendChild(imageErrorDiv);
                        } else {
                            if (!this.isImage(attachment.url)) return; // bruh
                            const img = document.createElement('img');
                            img.classList = ZLibrary.WebpackModules.getByProps('clickable').clickable;
                            img.messageId = messageId;
                            img.id = attachment.id; // USED FOR FINDING THE IMAGE THRU CONTEXT MENUS
                            img.idx = idx;
                            if (attachment.hidden) {
                                img.src = `https://i.clouds.tf/q2vy/r8q6.png#${record.message.channel_id},${img.id}`;
                                img.width = 200;
                            } else {
                                img.src = attachment.url;
                                img.width = this.clamp(attachment.width, 200, 650);
                            }
                            // img.style.minHeight = '104px'; // bruh?
                            if (record) {
                                img.addEventListener('click', () => {
                                    if (this.menu.deleteKeyDown) {
                                        this.deleteMessageFromRecords(messageId);
                                        this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                                        this.saveData();
                                        return;
                                    }
                                    this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop;
                                    this.createModal(
                                        {
                                            src: attachment.url + '?ML2=true', // self identify
                                            placeholder: attachment.url, // cute image here
                                            original: attachment.url,
                                            width: attachment.width,
                                            height: attachment.height,
                                            onClickUntrusted: e => e.openHref()
                                        },
                                        true
                                    );
                                });
                            }
                            img.onerror = () => {
                                img.src = '/assets/e0c782560fd96acd7f01fda1f8c6ff24.svg';
                                img.width = 200;
                                if (record) {
                                    this.nodeModules.request.head(attachment.url, (err, res) => {
                                        if (err || res.statusCode != 404) return;
                                        record.message.attachments[idx].url = 'ERROR';
                                        attemptToUseCachedImage(attachment.id, idx, attachment.hidden);
                                    });
                                }
                            };
                            divParent.appendChild(img);
                        }
                    };
                    for (let ii = 0; ii < message.attachments.length; ii++) handleCreateImage(message.attachments[ii], ii);
                }
                if (divParent.childElementCount < 2 && !message.content.length) return; // don't bother
                messageContent.appendChild(divParent);
                parent.appendChild(messageGroup);
            } catch (err) {
                ZLibrary.Logger.stacktrace(this.getName(), 'Error in populateParent', err);
            }
        };
        let i = 0;
        const addMore = () => {
            for (let added = 0; i < messages.length && (added < this.settings.renderCap || (this.menu.shownMessages != -1 && i < this.menu.shownMessages)); i++, added++) populate(i);
            handleMoreMessages();
            this.menu.shownMessages = i;
        };
        const handleMoreMessages = () => {
            if (i < messages.length) {
                const div = document.createElement('div');
                const moreButton = this.createButton('LOAD MORE', function() {
                    this.parentElement.remove();
                    addMore();
                });
                moreButton.style.width = '100%';
                moreButton.style.marginBottom = '20px';
                div.appendChild(moreButton);
                parent.appendChild(div);
            }
        };

        if (this.settings.renderCap) addMore();
        else for (; i < messages.length; i++) populate(i);
        this.processUserRequestQueue();
    }
    // >>-|| FILTERING ||-<<
    sortMessagesByAge(map) {
        // sort direction: new - old
        map.sort((a, b) => {
            const recordA = this.messageRecord[a];
            const recordB = this.messageRecord[b];
            if (!recordA || !recordB) return 0;
            let timeA = new Date(recordA.message.timestamp).getTime();
            let timeB = new Date(recordB.message.timestamp).getTime();
            if (recordA.edit_history && typeof recordA.edit_history[recordA.edit_history.length - 1].time !== 'string') timeA = recordA.edit_history[recordA.edit_history.length - 1].time;
            if (recordB.edit_history && typeof recordB.edit_history[recordB.edit_history.length - 1].time !== 'string') timeB = recordB.edit_history[recordB.edit_history.length - 1].time;
            if (recordA.delete_data && recordA.delete_data.time) timeA = recordA.delete_data.time;
            if (recordB.delete_data && recordB.delete_data.time) timeB = recordB.delete_data.time;
            return parseInt(timeB) - parseInt(timeA);
        });
    }
    getFilteredMessages() {
        let messages = [];

        const pushIdsIntoMessages = map => {
            for (let channel in map) {
                for (let messageIdIDX in map[channel]) {
                    messages.push(map[channel][messageIdIDX]);
                }
            }
        };
        const checkIsMentioned = map => {
            for (let channel in map) {
                for (let messageIdIDX in map[channel]) {
                    const messageId = map[channel][messageIdIDX];
                    const record = this.getSavedMessage(messageId);
                    if (!record) continue;
                    if (record.local_mentioned) {
                        messages.push(messageId);
                    }
                }
            }
        };

        if (this.menu.selectedTab == 'sent') {
            for (let i of this.cachedMessageRecord) {
                messages.push(i.id);
            }
        }
        if (this.menu.selectedTab == 'edited') pushIdsIntoMessages(this.editedMessageRecord);
        if (this.menu.selectedTab == 'deleted') pushIdsIntoMessages(this.deletedMessageRecord);
        if (this.menu.selectedTab == 'purged') pushIdsIntoMessages(this.purgedMessageRecord);
        if (this.menu.selectedTab == 'ghostpings') {
            checkIsMentioned(this.deletedMessageRecord);
            checkIsMentioned(this.editedMessageRecord);
            checkIsMentioned(this.purgedMessageRecord);
        }

        const filters = this.menu.filter.split(',');

        for (let i = 0; i < filters.length; i++) {
            const split = filters[i].split(':');
            if (split.length < 2) continue;

            const filterType = split[0].trim().toLowerCase();
            const filter = split[1].trim().toLowerCase();

            if (filterType == 'server' || filterType == 'guild')
                messages = messages.filter(x => {
                    const message = this.getMessageAny(x);
                    if (!message) return false;
                    const channel = this.tools.getChannel(message.channel_id);
                    const guild = this.tools.getServer(message.guild_id || (channel && channel.guild_id));
                    return (message.guild_id || (channel && channel.guild_id)) == filter || (guild && guild.name.toLowerCase().includes(filter.toLowerCase()));
                });

            if (filterType == 'channel')
                messages = messages.filter(x => {
                    const message = this.getMessageAny(x);
                    if (!message) return false;
                    const channel = this.tools.getChannel(message.channel_id);
                    return message.channel_id == filter || (channel && channel.name.toLowerCase().includes(filter.replace('#', '').toLowerCase()));
                });

            if (filterType == 'message' || filterType == 'content')
                messages = messages.filter(x => {
                    const message = this.getMessageAny(x);
                    return x == filter || (message && message.content.toLowerCase().includes(filter.toLowerCase()));
                });

            if (filterType == 'user')
                messages = messages.filter(x => {
                    const message = this.getMessageAny(x);
                    if (!message) return false;
                    const channel = this.tools.getChannel(message.channel_id);
                    const member = ZLibrary.DiscordModules.GuildMemberStore.getMember(message.guild_id || (channel && channel.guild_id), message.author.id);
                    return message.author.id == filter || message.author.username.toLowerCase().includes(filter.toLowerCase()) || (member && member.nick && member.nick.toLowerCase().includes(filter.toLowerCase()));
                });
        }

        if (this.menu.selectedTab != 'sent') {
            this.sortMessagesByAge(messages);
            if (this.settings.reverseOrder) messages.reverse(); // this gave me a virtual headache
        } else if (!this.settings.reverseOrder) messages.reverse(); // this gave me a virtual headache

        return messages;
    }
    // >>-|| REPOPULATE ||-<<
    refilterMessages() {
        const messagesDIV = document.getElementById('ml2-menu-messages');
        const original = messagesDIV.style.display;
        messagesDIV.style.display = 'none';
        while (messagesDIV.firstChild) messagesDIV.removeChild(messagesDIV.firstChild);
        this.menu.messages = this.getFilteredMessages();
        this.populateParent(messagesDIV, this.menu.messages);
        messagesDIV.style.display = original;
    }
    // >>-|| HEADER ||-<<
    openTab(tab) {
        const tabBar = document.getElementById('ml2-tabbar');
        if (!tabBar) return this.showToast(`Error switching to tab ${tab}!`, { type: 'error', timeout: 3000 });
        tabBar.querySelector('.ml2-tab-selected').classList.remove('ml2-tab-selected');
        tabBar.querySelector('#' + tab).classList.add('ml2-tab-selected');
        this.menu.selectedTab = tab;
        setTimeout(() => this.refilterMessages(), 0);
    }
    createHeader() {
        const classes = this.createHeader.classes;
        const createTab = (title, id) => {
            const tab = this.parseHTML(`<div id="${id}" class="${classes.itemTabBarItem} ml2-tab ${id == this.menu.selectedTab ? 'ml2-tab-selected' : ''}" role="button">${title}</div>`);
            tab.addEventListener('mousedown', () => this.openTab(id));
            return tab;
        };
        const tabBar = this.parseHTML(`<div class="${classes.tabBarContainer}"><div class="${classes.tabBar}" id="ml2-tabbar"></div></div>`);
        const tabs = tabBar.getElementsByClassName(classes.tabBarSingle)[0];
        tabs.appendChild(createTab('Sent', 'sent'));
        tabs.appendChild(createTab('Deleted', 'deleted'));
        tabs.appendChild(createTab('Edited', 'edited'));
        tabs.appendChild(createTab('Purged', 'purged'));
        tabs.appendChild(createTab('Ghost pings', 'ghostpings'));
        const measureWidth = el => {
            el = el.cloneNode(true);

            el.style.visibility = 'hidden';
            el.style.position = 'absolute';

            document.body.appendChild(el);
            let result = el.getBoundingClientRect().width;
            el.remove();
            return result;
        };
        const totalWidth = measureWidth(tabs) * 2 - 20;
        const wantedTabWidth = totalWidth / tabs.childElementCount;
        const wantedTabMargin = wantedTabWidth / 2;
        let tab = tabs.firstElementChild;
        while (tab) {
            tab.style.marginRight = '0px';
            const tabWidth = measureWidth(tab);
            if (tabWidth > wantedTabWidth) {
                ZLibrary.Logger.err(this.getName(), `What the shit? Tab ${tab} is massive!!`);
                tab = tab.nextElementSibling;
                continue;
            }
            tab.style.paddingRight = tab.style.paddingLeft = `${wantedTabMargin - tabWidth / 2}px`;
            tab = tab.nextElementSibling;
        }
        tabBar.style.marginRight = '20px';
        return tabBar;
    }
    createTextBox() {
        const classes = this.createTextBox.classes;
        let textBox = this.parseHTML(
            `<div class="${classes.inputWrapper}"><div class="${classes.inputMultiInput}"><div class="${classes.inputWrapper} ${classes.multiInputFirst}"><input class="${classes.inputDefaultMultiInputField}" name="username" type="text" placeholder="Message filter" maxlength="999" value="${this.menu.filter}" id="ml2-filter"></div><span tabindex="0" aria-label="Help!" class="${classes.questionMark}" role="button"><svg name="QuestionMark" class="${classes.icon}" aria-hidden="false" width="16" height="16" viewBox="0 0 24 24"><g fill="currentColor" fill-rule="evenodd" transform="translate(7 4)"><path d="M0 4.3258427C0 5.06741573.616438356 5.68539326 1.35616438 5.68539326 2.09589041 5.68539326 2.71232877 5.06741573 2.71232877 4.3258427 2.71232877 2.84269663 4.31506849 2.78089888 4.5 2.78089888 4.68493151 2.78089888 6.28767123 2.84269663 6.28767123 4.3258427L6.28767123 4.63483146C6.28767123 5.25280899 5.97945205 5.74719101 5.42465753 6.05617978L4.19178082 6.73595506C3.51369863 7.10674157 3.14383562 7.78651685 3.14383562 8.52808989L3.14383562 9.64044944C3.14383562 10.3820225 3.76027397 11 4.5 11 5.23972603 11 5.85616438 10.3820225 5.85616438 9.64044944L5.85616438 8.96067416 6.71917808 8.52808989C8.1369863 7.78651685 9 6.30337079 9 4.69662921L9 4.3258427C9 1.48314607 6.71917808 0 4.5 0 2.21917808 0 0 1.48314607 0 4.3258427zM4.5 12C2.5 12 2.5 15 4.5 15 6.5 15 6.5 12 4.5 12L4.5 12z"></path></g></svg></span></div></div>`
        );
        const inputEl = textBox.getElementsByTagName('input')[0];
        inputEl.addEventListener('focusout', e => {
            DOMTokenList.prototype.remove.apply(e.target.parentElement.parentElement.classList, classes.focused);
        });
        inputEl.addEventListener('focusin', e => {
            DOMTokenList.prototype.add.apply(e.target.parentElement.parentElement.classList, classes.focused);
        });
        const onUpdate = e => {
            if (this.menu.filterSetTimeout) clearTimeout(this.menu.filterSetTimeout);
            this.menu.filter = inputEl.value;
            const filters = this.menu.filter.split(',');
            // console.log(filters);
            if (!filters[0].length) return this.refilterMessages();
            this.menu.filterSetTimeout = setTimeout(() => {
                if (filters[0].length) {
                    for (let i = 0; i < filters.length; i++) {
                        const split = filters[i].split(':');
                        if (split.length < 2) return;
                    }
                }
                this.refilterMessages();
            }, 200);
        };
        inputEl.addEventListener('keyup', onUpdate); // maybe I can actually use keydown but it didn't work for me
        inputEl.addEventListener('paste', onUpdate);
        const helpButton = textBox.getElementsByClassName(classes.questionMarkSingle)[0];
        helpButton.addEventListener('click', () => {
            this.scrollPosition = document.getElementById('ml2-menu-messages').parentElement.parentElement.parentElement.scrollTop;
            const extraHelp = this.createButton('Logger help', () => this.showLoggerHelpModal());
            this.createModal({
                confirmText: 'OK',
                header: 'Filter help',
                size: ZLibrary.Modals.ModalSizes.LARGE,
                children: [
                    ZLibrary.ReactTools.createWrappedElement([
                        this.parseHTML(
                            `<div class="${this.multiClasses.defaultColor}">"server: <servername or serverid>" - Filter results with the specified server name or id.
                    "channel: <channelname or channelid>" - Filter results with the specified channel name or id.
                    "user: <username, nickname or userid>" - Filter results with the specified username, nickname or userid.
                    "message: <search or messageid>" or "content: <search or messageid>" - Filter results with the specified message content.

                    Separate the search tags with commas.
                    Example: server: tom's bd stuff, message: heck


                    Shortcut help:

                    "Ctrl + M" (default) - Open message log.
                    "Ctrl + N" (default) - Open message log with selected channel filtered.\n\n</div>`.replace(/\n/g, '</br>')
                        ),
                        extraHelp
                    ])
                ],
                red: false
            });
        });
        new ZeresPluginLibrary.EmulatedTooltip(helpButton, 'Help!', { side: 'top' });
        return textBox;
    }
    // >>-|| MENU MODAL CREATION ||-<<
    openWindow(type) {
        if (this.menu.open) {
            this.menu.scrollPosition = 0;
            if (type) this.openTab(type);
            return;
        }
        this.menu.open = true;
        if (type) this.menu.selectedTab = type;
        if (!this.menu.selectedTab) this.menu.selectedTab = 'deleted';
        const messagesDIV = this.parseHTML(`<div id="ml2-menu-messages"></div>`);
        const viewportHeight = document.getElementById('app-mount').getBoundingClientRect().height;
        messagesDIV.style.minHeight = viewportHeight * 0.514090909 + 'px'; // hack but ok
        //messagesDIV.style.display = 'none';
        this.createModal(
            {
                confirmText: 'Clear log',
                cancelText: 'Sort direction: ' + (!this.settings.reverseOrder ? 'new - old' : 'old - new'),
                header: ZLibrary.ReactTools.createWrappedElement([this.createTextBox(), this.createHeader()]),
                size: ZLibrary.Modals.ModalSizes.LARGE,
                children: [ZLibrary.ReactTools.createWrappedElement([messagesDIV])],
                ml2Data: true
            },
            false,
            'ML2-MENU'
        );
        let loadAttempts = 0;
        const loadMessages = () => {
            loadAttempts++;
            try {
                this.refilterMessages();
            } catch (e) {
                if (loadAttempts > 4) {
                    this.showToast(`Couldn't load menu messages! Report this issue to Lighty, error info is in console`, { type: 'error', timeout: 10000 });
                    ZLibrary.Logger.stacktrace(this.getName(), 'Failed loading menu', e);
                    return;
                }
                setTimeout(() => loadMessages(), 100);
            }
        };
        setTimeout(() => loadMessages(), 100);
    }
    /* ==================================================-|| END MENU ||-================================================== */
    /* ==================================================-|| START CONTEXT MENU ||-================================================== */
    handleContextMenu(thisObj, args, returnValue) {
        if (!returnValue) return returnValue;
        // console.log(thisObj, args, returnValue);
        const type = thisObj.props.type;
        const newItems = [];
        const addElement = (label, callback) => {
            newItems.push(
                ZLibrary.DiscordModules.React.createElement(this.ContextMenuItem, {
                    label: label,
                    action: () => {
                        this.ContextMenuActions.closeContextMenu();
                        if (callback) callback();
                    }
                })
            );
        };

        const handleWhiteBlackList = id => {
            const whitelistIdx = this.settings.whitelist.findIndex(m => m === id);
            const blacklistIdx = this.settings.blacklist.findIndex(m => m === id);
            if (whitelistIdx == -1 && blacklistIdx == -1) {
                addElement(`Add to whitelist`, () => {
                    this.settings.whitelist.push(id);
                    this.saveSettings();
                    this.showToast('Added!', { type: 'success' });
                });
                addElement(`Add to blacklist`, () => {
                    this.settings.blacklist.push(id);
                    this.saveSettings();
                    this.showToast('Added!', { type: 'success' });
                });
            } else if (whitelistIdx != -1) {
                addElement(`Remove from whitelist`, () => {
                    this.settings.whitelist.splice(whitelistIdx, 1);
                    this.saveSettings();
                    this.showToast('Removed!', { type: 'success' });
                });
                addElement(`Move to blacklist`, () => {
                    this.settings.whitelist.splice(whitelistIdx, 1);
                    this.settings.blacklist.push(id);
                    this.saveSettings();
                    this.showToast('Moved!', { type: 'success' });
                });
            } else {
                addElement(`Remove from blacklist`, () => {
                    this.settings.blacklist.splice(blacklistIdx, 1);
                    this.saveSettings();
                    this.showToast('Removed!', { type: 'success' });
                });
                addElement(`Move to whitelist`, () => {
                    this.settings.blacklist.splice(blacklistIdx, 1);
                    this.settings.whitelist.push(id);
                    this.saveSettings();
                    this.showToast('Moved!', { type: 'success' });
                });
            }
        };
        // image has no type property
        if (!type) {
            if (!this.menu.open) return;
            let matched;
            let isCached = false;
            if (thisObj.props.src && thisObj.props.src.startsWith('data:image/png')) {
                const cut = thisObj.props.src.substr(0, 100);
                matched = cut.match(/;(\d+);(\d+);/);
                isCached = true;
            } else {
                matched = thisObj.props.src.match(/.*ments\/(\d+)\/(\d+)\//);
                if (!matched) matched = thisObj.props.src.match(/r8q6.png#(\d+),(\d+)/);
            }
            if (!matched) return;
            const channelId = matched[1];
            const attachmentId = matched[2];
            const element = document.getElementById(attachmentId);
            if (!element) return;
            const attachmentIdx = element.idx;
            const record = this.getSavedMessage(element.messageId);
            if (!record) return;
            const attachmentRecord = this.imageCacheRecord[attachmentId];
            addElement('Save to folder', () => {
                const { dialog } = this.nodeModules.electron.remote;
                const dir = dialog.showSaveDialog({
                    defaultPath: record.message.attachments[attachmentIdx].filename
                });
                if (!dir) return;
                const attemptToUseCached = () => {
                    const srcFile = `${this.imageCacheDir}/${attachmentId}${(attachmentRecord && attachmentRecord.extension) || 'fuck'}`;
                    if (!attachmentRecord || !this.nodeModules.fs.existsSync(srcFile)) return this.showToast('Image does not exist locally!', { type: 'error', timeout: 5000 });
                    this.nodeModules.fs.copyFileSync(srcFile, dir);
                    this.showToast('Saved!', { type: 'success' });
                };
                if (isCached) {
                    attemptToUseCached();
                } else {
                    const req = this.nodeModules.request(record.message.attachments[attachmentIdx].url);
                    req.on('response', res => {
                        if (res.statusCode == 200) {
                            req.pipe(this.nodeModules.fs.createWriteStream(dir))
                                .on('finish', () => this.showToast('Saved!', { type: 'success' }))
                                .on('error', () => this.showToast('Failed to save! No permissions.', { type: 'error', timeout: 5000 }));
                        } else if (res.statusCode == 404) {
                            this.showToast('Image does not exist! Attempting to use local image cache.', { type: 'error' });
                            attemptToUseCached();
                        } else {
                            this.showToast('Unknown error, attempting to use local image cache.', { type: 'error' });
                            attemptToUseCached();
                        }
                    });
                }
            });
            addElement('Copy to clipboard', () => {
                const { clipboard, nativeImage } = this.nodeModules.electron;
                const attemptToUseCached = () => {
                    const srcFile = `${this.imageCacheDir}/${attachmentId}${(attachmentRecord && attachmentRecord.extension) || 'fuck'}`;
                    if (!attachmentRecord || !this.nodeModules.fs.existsSync(srcFile)) return this.showToast('Image does not exist locally!', { type: 'error', timeout: 5000 });
                    clipboard.write({ image: srcFile });
                    this.showToast('Copied!', { type: 'success' });
                };
                if (isCached) {
                    attemptToUseCached();
                } else {
                    const path = require('path');
                    const process = require('process');
                    // ImageToClipboard by Zerebos
                    this.nodeModules.request({ url: record.message.attachments[attachmentIdx].url, encoding: null }, (error, response, buffer) => {
                        if (error || response.statusCode != 200) {
                            this.showToast('Failed to copy. Image may not exist. Attempting to use local image cache.', { type: 'error' });
                            attemptToUseCached();
                            return;
                        }
                        if (process.platform === 'win32' || process.platform === 'darwin') {
                            clipboard.write({ image: nativeImage.createFromBuffer(buffer) });
                        } else {
                            const file = path.join(process.env.HOME, 'ml2temp.png');
                            this.nodeModules.fs.writeFileSync(file, buffer, { encoding: null });
                            clipboard.write({ image: file });
                            this.nodeModules.fs.unlinkSync(file);
                        }
                        this.showToast('Copied!', { type: 'success' });
                    });
                }
            });
            addElement('Jump to message', () => {
                this.jumpToMessage(channelId, element.messageId, record.message.guild_id);
            });
            if (record.delete_data && record.delete_data.hidden) {
                addElement('Unhide deleted message', () => {
                    record.delete_data.hidden = false;
                    this.invalidateChannelCache(record.message.channel_id); // good idea?
                    this.cacheChannelMessages(record.message.channel_id);
                    this.saveData();
                    this.showToast('Unhidden!', { type: 'success' });
                });
            }
            if (record.edit_history && record.edits_hidden) {
                addElement('Unhide deleted message', () => {
                    record.edits_hidden = false;
                    this.invalidateChannelCache(record.message.channel_id); // good idea?
                    this.cacheChannelMessages(record.message.channel_id);
                    this.saveData();
                    this.showToast('Unhidden!', { type: 'success' });
                });
            }
            addElement('Remove from log', () => {
                let invalidatedChannelCache = false;
                if ((record.edit_history && !record.edits_hidden) || record.delete_data) this.invalidateChannelCache((invalidatedChannelCache = record.message.channel_id));
                this.deleteMessageFromRecords(element.messageId);
                this.refilterMessages(); // I don't like calling that, maybe figure out a way to animate it collapsing on itself smoothly
                if (invalidatedChannelCache) this.cacheChannelMessages(invalidatedChannelCache);
                this.saveData();
            });
            if (!thisObj.props.src.startsWith('https://i.clouds.tf/q2vy/r8q6.png')) {
                addElement('Hide image from log', () => {
                    record.message.attachments[attachmentIdx].hidden = true;
                    element.src = `https://i.clouds.tf/q2vy/r8q6.png#${channelId},${attachmentId}`;
                    element.width = 200;
                });
            } else {
                addElement('Unhide image from log', () => {
                    record.message.attachments[attachmentIdx].hidden = false;
                    element.src = isCached ? attachmentRecord && attachmentRecord.thumbnail : record.message.attachments[attachmentIdx].url;
                    element.width = isCached ? 256 : this.clamp(record.message.attachments[attachmentIdx].width, 200, 650);
                });
            }
        } else if (type == 'MESSAGE_MAIN') {
            addElement('Open logs', () => this.openWindow());
            const messageId = thisObj.props.message.id;
            const channelId = thisObj.props.channel.id;
            const record = this.messageRecord[messageId];
            if (record) {
                /*
                addElement('Show in menu', () => {
                    this.menu.filter = `message:${messageId}`;
                    this.openWindow();
                }); */
                if (record.delete_data) {
                    const options = returnValue.props.children.find(m => m.props.children && m.props.children.length > 5);
                    options.props.children.splice(0, options.props.children.length);
                    addElement('Hide deleted message', () => {
                        ZLibrary.WebpackModules.find(m => m.dispatch).dispatch({
                            type: 'MESSAGE_DELETE',
                            id: messageId,
                            channelId: channelId,
                            ML2: true // ignore ourselves lol, it's already deleted
                            // on a side note, probably does nothing if we don't ignore
                        });
                        this.showToast('Hidden!', { type: 'success' });
                        record.delete_data.hidden = true;
                        this.saveData();
                    });
                }
                if (record.edit_history) {
                    if (record.edits_hidden) {
                        addElement('Unhide edits', () => {
                            record.edits_hidden = false;
                            this.saveData();
                            this.updateMessages();
                        });
                    } else {
                        let target = thisObj.props.target;
                        while (target.className.indexOf(this.style.edited) === -1) target = target.parentElement;
                        const parent = target.parentElement;
                        const editNum = target.firstChild.editNum;
                        if (typeof editNum === 'number') {
                            addElement('Delete edit', () => {
                                target.remove();
                                if (parent.childElementCount === 1) {
                                    const editedTag = document.createElement('time');
                                    editedTag.className = this.multiClasses.edited;
                                    editedTag.innerText = '(edited)';
                                    parent.firstChild.firstChild.appendChild(editedTag);
                                }
                                this.deleteEditedMessageFromRecord(messageId, editNum);
                            });
                        }
                        addElement('Hide edits', () => {
                            let parent = thisObj.props.target.parentElement;
                            while (!parent.classList.contains(this.classes.markup)) parent = parent.parentElement;
                            parent.classList.remove(this.style.edited);
                            parent.edits = 0;
                            while (parent.firstChild) parent.removeChild(parent.firstChild);
                            /*                             parent = parent.parentElement;
                            parent.removeChild(parent.firstChild); */
                            const newMarkup = this.formatMarkup(record.message.content, record.message.channel_id).firstChild.firstChild;
                            newMarkup.className = this.multiClasses.markup;
                            const editedTag = document.createElement('time');
                            editedTag.className = this.multiClasses.edited;
                            editedTag.innerText = '(edited)';
                            editedTag.addEventListener('click', () => {
                                this.menu.filter = `message:${messageId}`;
                                this.openWindow('edited');
                            });
                            parent.appendChild(newMarkup);
                            parent.appendChild(editedTag);
                            record.edits_hidden = true;
                            this.saveData();
                        });
                    }
                }
            }
        } else if (type == 'GUILD_ICON_BAR') {
            addElement('Open log for guild', () => {
                this.menu.filter = `server:${thisObj.props.guild.id}`;
                this.openWindow();
            });
            handleWhiteBlackList(thisObj.props.guild.id);
        } else if (type == 'CHANNEL_LIST_TEXT') {
            addElement('Open log for channel', () => {
                this.menu.filter = `channel:${thisObj.props.channel.id}`;
                this.openWindow();
            });
            handleWhiteBlackList(thisObj.props.channel.id);
        } else if (type == 'USER_PRIVATE_CHANNELS_MESSAGE' || type == 'USER_PRIVATE_CHANNELS' || type == 'USER_CHANNEL_MESSAGE') {
            if (!this.menu.open) {
                addElement('Open logs', () => this.openWindow());
                addElement('Open log for user', () => {
                    this.menu.filter = `user:${thisObj.props.user.id}`;
                    this.openWindow();
                });
                addElement(`Open log for ${type == 'USER_CHANNEL_MESSAGE' ? 'channel' : 'DM'}`, () => {
                    this.menu.filter = `channel:${thisObj.props.channelId}`;
                    this.openWindow();
                });
            }
            if (type != 'USER_CHANNEL_MESSAGE') handleWhiteBlackList(thisObj.props.channelId);
            returnValue.props.children.props.children.props.children.push(
                ZLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, {
                    children: [ZLibrary.DiscordModules.React.createElement(this.SubMenuItem, { label: 'Message logger', render: newItems, invertChildY: true })]
                })
            );
            return;
        }
        if (!newItems.length) return returnValue;
        returnValue.props.children.push(
            ZLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, {
                children: [ZLibrary.DiscordModules.React.createElement(this.SubMenuItem, { label: 'Message logger', render: newItems, invertChildY: true })]
            })
        );
    }
    /* ==================================================-|| END CONTEXT MENU ||-================================================== */
}
/*@end @*/
