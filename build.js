const fs = require('fs');
const pluginsDir = fs.readdirSync('public/plugins/');
let result = '';
for (let pluginFile of pluginsDir) {
    const plugin = fs.readFileSync('public/plugins/' + pluginFile, 'utf8');
    const name = plugin.match(/getName\(\) {\n\s+return ['"](.*)['"];\n\s+}/)[1];
    const description = plugin.match(/getDescription\(\) {\n\s+return ['"](.*)['"];\n\s+}/)[1];
    const version = plugin.match(/getVersion\(\) {\n\s+return ['"](.*)['"];\n\s+}/)[1];
    result += `<div id="${pluginFile.substr(0, pluginFile.length - 10)}">
            <div class="mdl-card mdl-cell mdl-cell--12-col">
                <div class="mdl-card__media mdl-color-text--grey-50" />
                <h4>${name}</h4>
            </div>
            <div class="mdl-card__supporting-text">
                ${description}
            </div>
            <div class="mdl-card__supporting-text sub-text">
                Version: ${version}
            </div>
            <div class="mdl-color-text--white-600 mdl-card__buttons">
                <a href="plugins/${pluginFile}" download
                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    download
                </a>
                <a href="https://gitlab.com/_Lighty_/bdstuff/blob/master/public/plugins/${pluginFile}" target="_blank"
                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">
                    view source
                </a>
            </div>
        </div>
        `;
}
let template = fs.readFileSync('public/index_template.html', 'utf8');
template = template.replace('{PLUGINS}', result);
fs.writeFileSync('public/index.html', template, 'utf8');
