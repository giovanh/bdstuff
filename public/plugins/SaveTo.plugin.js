//META{"name":"SaveTo","source":"https://gitlab.com/_Lighty_/bdstuff/blob/master/public/plugins/SaveTo.plugin.js","website":"https://_lighty_.gitlab.io/bdstuff/?plugin=SaveTo"}*//
class SaveTo {
    getName() {
        return 'SaveTo';
    }
    getVersion() {
        return '0.8.3';
    }
    getAuthor() {
        return 'Lighty';
    }
    getDescription() {
        return 'Allows you to save images, videos, profile icons and server icons to any folder. WIP plugin.';
    }
    load() {}
    start() {
        let onLoaded = () => {
            try {
                if (!global.ZeresPluginLibrary) setTimeout(() => onLoaded(), 1000);
                else this.initialize();
            } catch (err) {
                ZLibrary.Logger.stacktrace(this.getName(), 'Failed to start!', err);
                ZLibrary.Logger.err(this.getName(), `If you cannot solve this yourself, contact ${this.getAuthor()} and provide the errors shown here.`);
                this.stop();
                BdApi.showToast(`[${this.getName()}] Failed to start! Check console (CTRL + SHIFT + I, click console tab) for more error info.`, { type: 'error', timeout: 10000 });
            }
        };
        const getDir = () => {
            // from Zeres Plugin Library, copied here as ZLib may not be available at this point
            const process = require('process');
            const path = require('path');
            if (process.env.injDir) return path.resolve(process.env.injDir, 'plugins/');
            switch (process.platform) {
                case 'win32':
                    return path.resolve(process.env.appdata, 'BetterDiscord/plugins/');
                case 'darwin':
                    return path.resolve(process.env.HOME, 'Library/Preferences/', 'BetterDiscord/plugins/');
                default:
                    return path.resolve(process.env.XDG_CONFIG_HOME ? process.env.XDG_CONFIG_HOME : process.env.HOME + '/.config', 'BetterDiscord/plugins/');
            }
        };
        this.pluginDir = getDir();
        let libraryOutdated = false;
        // I'm sick and tired of people telling me my plugin doesn't work and it's cause zlib is outdated, ffs
        if (!global.ZLibrary || !global.ZeresPluginLibrary || (bdplugins.ZeresPluginLibrary && (libraryOutdated = ZeresPluginLibrary.PluginUpdater.defaultComparator(bdplugins.ZeresPluginLibrary.plugin._config.info.version, '1.2.6')))) {
            const title = libraryOutdated ? 'Library outdated' : 'Library Missing';
            const ModalStack = BdApi.findModuleByProps('push', 'update', 'pop', 'popWithKey');
            const TextElement = BdApi.findModuleByProps('Sizes', 'Weights');
            const ConfirmationModal = BdApi.findModule(m => m.defaultProps && m.key && m.key() == 'confirm-modal');
            const confirmedDownload = () => {
                require('request').get('https://rauenzi.github.io/BDPluginLibrary/release/0PluginLibrary.plugin.js', async (error, response, body) => {
                    if (error) return require('electron').shell.openExternal('https://betterdiscord.net/ghdl?url=https://raw.githubusercontent.com/rauenzi/BDPluginLibrary/master/release/0PluginLibrary.plugin.js');
                    require('fs').writeFile(require('path').join(this.pluginDir, '0PluginLibrary.plugin.js'), body, () => {
                        setTimeout(() => {
                            if (!global.bdplugins.ZeresPluginLibrary) return BdApi.alert('Notice', `Due to you using EnhancedDiscord instead of BetterDiscord, you'll have to reload your Discord before ${this.getName()} starts working. Just press CTRL + R to reload and ${this.getName()} will begin to work!`);
                            onLoaded();
                        }, 1000);
                    });
                });
            };
            if (!ModalStack || !ConfirmationModal || !TextElement) {
                BdApi.alert('Uh oh', `Looks like you${libraryOutdated ? 'r Zeres Plugin Library was outdated!' : ' were missing Zeres Plugin Library!'} Also, failed to show a modal, so it has been ${libraryOutdated ? 'updated' : 'downloaded and loaded'} automatically.`);
                confirmedDownload();
                return;
            }
            ModalStack.push(props => {
                return BdApi.React.createElement(
                    ConfirmationModal,
                    Object.assign(
                        {
                            header: title,
                            children: [TextElement({ color: TextElement.Colors.PRIMARY, children: [`The library plugin needed for ${this.getName()} is ${libraryOutdated ? 'outdated' : 'missing'}. Please click Download Now to ${libraryOutdated ? 'update' : 'install'} it.`] })],
                            red: false,
                            confirmText: 'Download Now',
                            cancelText: 'Cancel',
                            onConfirm: () => confirmedDownload()
                        },
                        props
                    )
                );
            });
        } else onLoaded();
    }
    stop() {
        try {
            this.shutdown();
        } catch (err) {
            ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Failed to stop!', err);
        }
    }
    getChanges() {
        return [
            {
                title: 'added',
                type: 'added',
                items: ['Added SaveTo.']
            } /* ,
            {
                title: 'Improved',
                type: 'improved',
                items: [
                    'Links in edited messages are darkened like normal text to differentiate between edit and current message',
                    'Edited message color is no longer hardcoded, it is now simply normal text color but darkened, to match theme colors.',
                    'Toasts no longer show for local user in DMs (you did it, why show it?).',
                    'Ghost ping toast no longer shows if you are in the channel.'
                ]
            } */ /*
            {
                title: 'fixes',
                type: 'fixed',
                items: ['Fixed erroring out when search is open or if someone joins.', 'Fixed tooltips on edited then deleted messages.', 'Fix breaking Discord lmao SEASON 2 EPISODE 2 PART 2']
            }  */,

            {
                title: 'In the works',
                type: 'progress',
                items: ['Will add more options once the library is fixed like sorting and filename type.', 'Will add option to save emotes.']
            }
        ];
    }
    initialize() {
        ZeresPluginLibrary.PluginUpdater.checkForUpdate(this.getName(), this.getVersion(), `https://_lighty_.gitlab.io/bdstuff/plugins/${this.getName()}.plugin.js`);
        let defaultSettings = {
            fileNameType: 0,
            sortMode: 0,
            folders: [],
            displayUpdateNotes: true,
            contextMenuOnBottom: true,
            versionInfo: ''
        };
        this.settings = this.loadData(this.getName(), defaultSettings);

        if (this.settings.versionInfo !== this.getVersion() && this.settings.displayUpdateNotes) {
            ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges());
            this.settings.versionInfo = this.getVersion();
            this.saveSettings();
        }

        const emojiItemSelector = new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('row', 'emojiPicker').emojiItem).toString();
        const guildIconImageSelector = new ZLibrary.DOMTools.Selector(ZLibrary.WebpackModules.getByProps('guildIconImage').guildIconImage).toString();
        document.body.addEventListener(
            'contextmenu',
            (this.contextmenuEventListener = e => {
                const target = e.target;
                let isGuild = false;
                let url;
                if (target.matches(emojiItemSelector)) {
                    url = target.style.backgroundImage.split('"')[1];
                } else if ((isGuild = target.matches(guildIconImageSelector))) {
                    url = ZLibrary.ReactTools.getOwnerInstance(target.parentElement)
                        .props.guild.getIconURL()
                        .split('?')[0];
                } else return;
                if (url.indexOf('/a_') !== -1) url = url.replace('.webp', '.gif');
                else url = url.replace('.webp', '.png');
                if (isGuild) url += '?size=2048';
                return; // error, sub context menus are broken currently
                console.log('URL', url);
                const menu = new ZeresPluginLibrary.ContextMenu.Menu();
                const createSubMenu = (name, items, callback) => {
                    const subMenu = new ZeresPluginLibrary.ContextMenu.Menu();
                    const subgroup = new ZeresPluginLibrary.ContextMenu.ItemGroup();
                    subgroup.addItems(...items);
                    subMenu.addItems(subgroup);
                    for (let ch of subgroup.element.children) ch.addClass('clickable-11uBi- da-clickable'); // HACK
                    subMenu.element.style.display = 'inline-block';
                    subMenu.element.style.position = 'fixed';
                    subMenu.element.style.marginLeft = 0;
                    const menu = new ZeresPluginLibrary.ContextMenu.SubMenuItem(name, subMenu);
                    if (callback) menu.element.addEventListener('click', () => callback());
                    menu.element.style.overflow = 'hidden';
                    console.log(menu.element.firstElementChild);
                    return menu;
                };
                const createItem = (name, callback) => {
                    const item = new ZeresPluginLibrary.ContextMenu.TextItem(name);
                    if (callback) item.element.addEventListener('click', () => callback());
                    return item;
                };
                const constructedMenu = this.constructMenu('', 'n*****s', createSubMenu, createItem);
                const subgroup = new ZeresPluginLibrary.ContextMenu.ItemGroup();
                subgroup.addItems(constructedMenu);
                menu.addItems(subgroup);
                menu.element.style.position = 'absolute';
                menu.element.style.zIndex = '99999';
                for (let ch of menu.element.children) ch.addClass('clickable-11uBi- da-clickable'); // HACK
                menu.show(e.clientX, e.clientY);
            })
        );

        this.ContextMenuGroup = ZeresPluginLibrary.DiscordModules.ContextMenuItemsGroup;
        this.ContextMenuItem = ZeresPluginLibrary.DiscordModules.ContextMenuItem;
        this.ContextMenuActions = ZeresPluginLibrary.DiscordModules.ContextMenuActions;
        this.SubMenuItem = ZeresPluginLibrary.WebpackModules.find(m => m.default && m.default.displayName && m.default.displayName.includes('SubMenuItem')).default;

        const GuildContextMenu = ZeresPluginLibrary.WebpackModules.getByDisplayName('GuildContextMenu');
        const NativeLinkGroup = ZeresPluginLibrary.WebpackModules.getByDisplayName('NativeLinkGroup');
        const UserContextMenu = ZeresPluginLibrary.WebpackModules.getByDisplayName('UserContextMenu');

        this.unpatches = [];

        const patchContexts = menu => {
            this.unpatches.push(ZeresPluginLibrary.Patcher.after(this.getName(), menu.prototype, 'render', (thisObj, args, returnValue) => this.handleContextMenu(thisObj, args, returnValue)));
        };

        for (let menu of [GuildContextMenu, NativeLinkGroup, UserContextMenu]) patchContexts(menu);

        this.openSaveDialog = require('electron').remote.dialog.showSaveDialog;
        this.openOpenDialog = require('electron').remote.dialog.showOpenDialog;
        this.openItem = require('electron').shell.openItem;
        this.fs = require('fs');
        this.request = require('request');
        this.path = require('path');

        this.createModal.confirmationModal = ZeresPluginLibrary.DiscordModules.ConfirmationModal;

        this.createButton.classes = {
            button: (function() {
                let buttonData = ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand');
                return `${buttonData.button} ${buttonData.lookFilled} ${buttonData.colorBrand} ${buttonData.sizeSmall} ${buttonData.grow}`;
            })(),
            buttonContents: ZeresPluginLibrary.WebpackModules.getByProps('button', 'colorBrand').contents
        };
    }
    shutdown() {
        const tryUnpatch = fn => {
            try {
                // things can bug out, best to reload tbh, should maybe warn the user?
                fn();
            } catch (e) {
                ZeresPluginLibrary.Logger.stacktrace(this.getName(), 'Error unpatching', e);
            }
        };
        if (this.unpatches) for (let unpatch of this.unpatches) tryUnpatch(unpatch);
        if (this.contextmenuEventListener) document.body.removeEventListener('contextmenu', this.contextmenuEventListener);
    }
    loadData(name, construct = {}) {
        try {
            return Object.assign(construct, BdApi.getData(name, 'settings')); // zeres library turns all arrays into objects.. for.. some reason?
        } catch (e) {
            return {}; // error handling not done here
        }
    }
    saveSettings() {
        ZeresPluginLibrary.PluginUtilities.saveSettings(this.getName(), this.settings);
    }
    buildSetting(data) {
        // compied from ZLib actually
        const { name, note, type, value, onChange, id } = data;
        let setting = null;
        if (type == 'color') {
            setting = new ZeresPluginLibrary.Settings.ColorPicker(name, note, value, onChange, { disabled: data.disabled }); // DOESN'T WORK, REEEEEEEEEE
        } else if (type == 'dropdown') {
            setting = new ZeresPluginLibrary.Settings.Dropdown(name, note, value, data.options, onChange);
        } else if (type == 'file') {
            setting = new ZeresPluginLibrary.Settings.FilePicker(name, note, onChange);
        } else if (type == 'keybind') {
            setting = new ZeresPluginLibrary.Settings.Keybind(name, note, value, onChange);
        } else if (type == 'radio') {
            setting = new ZeresPluginLibrary.Settings.RadioGroup(name, note, value, data.options, onChange, { disabled: data.disabled });
        } else if (type == 'slider') {
            const options = {};
            if (typeof data.markers != 'undefined') options.markers = data.markers;
            if (typeof data.stickToMarkers != 'undefined') options.stickToMarkers = data.stickToMarkers;
            setting = new ZeresPluginLibrary.Settings.Slider(name, note, data.min, data.max, value, onChange, options);
        } else if (type == 'switch') {
            setting = new ZeresPluginLibrary.Settings.Switch(name, note, value, onChange, { disabled: data.disabled });
        } else if (type == 'textbox') {
            setting = new ZeresPluginLibrary.Settings.Textbox(name, note, value, onChange, { placeholder: data.placeholder || '' });
        }
        if (id) setting.getElement().id = id;
        return setting;
    }
    createSetting(data) {
        const current = Object.assign({}, data);
        if (!current.onChange) {
            current.onChange = value => {
                this.settings[current.id] = value;
                if (current.callback) current.callback(value);
            };
        }
        if (typeof current.value === 'undefined') current.value = this.settings[current.id];
        return this.buildSetting(current);
    }
    getSettingsPanel() {
        // todo, sort out the menu
        const list = [];
        list.push(
            this.createSetting({
                name: 'Context menu option at the bottom instead of top',
                id: 'contextMenuOnBottom',
                type: 'switch'
            })
        );
        list.push(
            this.createSetting({
                name: 'Show update notes on update',
                id: 'displayUpdateNotes',
                type: 'switch'
            })
        );

        const div = document.createElement('div');
        div.id = 'st-settings-buttonbox';
        div.style.display = 'inline-flex';
        div.appendChild(this.createButton('Changelog', () => ZeresPluginLibrary.Modals.showChangelogModal(this.getName() + ' Changelog', this.getVersion(), this.getChanges())));
        div.appendChild(this.createButton('Donate', () => this.nodeModules.electron.shell.openExternal('https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DZUL9UZDDRLB8&source=url')));
        let button = div.firstElementChild;
        while (button) {
            button.style.marginRight = button.style.marginLeft = `5px`;
            button = button.nextElementSibling;
        }

        list.push(div);

        return ZeresPluginLibrary.Settings.SettingPanel.build(_ => this.saveSettings(), ...list);
    }
    createButton(label, callback) {
        const classes = this.createButton.classes;
        const ret = ZLibrary.DOMTools.parseHTML(`<button type="button" class="${classes.button}"><div class="${classes.buttonContents}">${label}</div></button>`);
        if (callback) ret.addEventListener('click', callback);
        return ret;
    }
    createModal(options, name) {
        const modal = this.createModal.confirmationModal;
        ZeresPluginLibrary.DiscordModules.ModalStack.push(function(props) {
            return ZeresPluginLibrary.DiscordModules.React.createElement(modal, Object.assign(options, props));
        });
    }
    constructMenu(url, type, createSubMenu, createItem) {
        const subItems = [];
        const folderSubMenus = [];
        const formattedurl = this.formatURL(url, type === 'Icon' || type === 'Avatar');

        const saveFile = (path, openOnSave) => {
            const req = this.request(formattedurl.url);
            req.on('response', res => {
                if (res.statusCode == 200) {
                    req.pipe(this.fs.createWriteStream(path))
                        .on('finish', () => {
                            if (openOnSave) this.openItem(path);
                            BdApi.showToast('Saved!', { type: 'success' });
                        })
                        .on('error', () => BdApi.showToast('Failed to save! No permissions.', { type: 'error', timeout: 5000 }));
                } else if (res.statusCode == 404) BdApi.showToast('Image does not exist!', { type: 'error' });
                else BdApi.showToast('Unknown error.', { type: 'error' });
            });
        };

        const folderSubMenu = folder => {
            return createSubMenu(
                folder.name,
                [
                    createItem('Remove Folder', () => {
                        const index = this.settings.folders.findIndex(m => m === folder);
                        if (index === -1) return BdApi.showToast("Fatal error! Attempted to remove a folder that doesn't exist!", { type: 'error', timeout: 5000 });
                        this.settings.folders.splice(index, 1);
                        this.saveSettings();
                        BdApi.showToast('Removed!', { type: 'success' });
                    }),
                    createItem('Open Folder', () => {
                        this.openItem(folder.path);
                    }),
                    createItem('Save', () => {
                        const path = folder.path + `/${formattedurl.fileName}`;
                        saveFile(path);
                    }),
                    createItem('Save As...', () => {
                        const path = this.openSaveDialog({
                            defaultPath: folder.path + `/${formattedurl.fileName}`
                        });
                        if (!path) return BdApi.showToast('Maybe next time.');
                        saveFile(path);
                    }),
                    createItem('Save And Open', () => {
                        const path = folder.path + `/${formattedurl.fileName}`;
                        saveFile(path, true);
                    }),
                    createItem('Edit', () => {
                        let ref1;
                        let ref2;
                        const saveFolder = () => {
                            folder.name = ref1.props.value;
                            folder.path = ref2.props.value;
                        };
                        const selectFolder = () => {
                            const path = this.openOpenDialog({ title: 'Select folder', properties: ['openDirectory', 'createDirectory'] });
                            if (!path) return BdApi.showToast('Maybe next time.');
                            let idx;
                            if ((idx = this.settings.folders.findIndex(m => m.path === path[0])) !== -1) return BdApi.showToast(`Folder already exists as ${this.settings.folders[idx].name}!`, { type: 'error', timeout: 5000 });
                            ref2.props.value = path[0];
                            ref2.forceUpdate();
                        };
                        this.createModal({
                            confirmText: 'Save',
                            cancelText: 'Cancel',
                            header: 'Edit folder',
                            size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
                            red: false,
                            children: [
                                ZLibrary.DiscordModules.React.createElement(
                                    ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                                    {
                                        title: 'Folder name',
                                        className: ZLibrary.WebpackModules.getByProps('input', 'card').input
                                    },
                                    ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                                        ref: e => {
                                            ref1 = e;
                                        },
                                        value: folder.name,
                                        onChange: value => {
                                            ref1.props.value = value;
                                            ref1.forceUpdate();
                                        },
                                        autoFocus: 1
                                    })
                                ),
                                ZLibrary.DiscordModules.React.createElement(
                                    ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                                    {
                                        title: 'Folder path',
                                        className: ZLibrary.WebpackModules.getByProps('input', 'card').input
                                    },
                                    ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                                        ref: e => {
                                            ref2 = e;
                                        },
                                        value: folder.path,
                                        onChange: value => {
                                            ref2.props.value = value;
                                            ref2.forceUpdate();
                                        }
                                    })
                                ),
                                ZLibrary.DiscordModules.React.createElement(
                                    ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink'),
                                    {
                                        color: ZLibrary.WebpackModules.find(m => m.Link && m.Link.displayName === 'ButtonLink').Colors.BRAND,
                                        style: {
                                            width: '100%'
                                        },
                                        onClick: () => selectFolder()
                                    },
                                    'Browse'
                                )
                            ],
                            onConfirm: () => saveFolder()
                        });
                    })
                ],
                () => {
                    const path = folder.path + `/${formattedurl.fileName}`;
                    saveFile(path);
                }
            );
        };
        for (const folder of this.settings.folders) folderSubMenus.push(folderSubMenu(folder));
        subItems.push(
            ...folderSubMenus,
            createItem('Add Folder', () => {
                const path = this.openOpenDialog({ title: 'Add folder', properties: ['openDirectory', 'createDirectory'] });
                if (!path) return BdApi.showToast('Maybe next time.');
                let idx;
                if ((idx = this.settings.folders.findIndex(m => m.path === path[0])) !== -1) return BdApi.showToast(`Folder already exists as ${this.settings.folders[idx].name}!`, { type: 'error', timeout: 5000 });
                const folderName = this.path.basename(path[0]);
                let ref;
                const saveFolder = () => {
                    this.settings.folders.push({
                        path: path[0],
                        name: ref.props.value,
                        position: this.settings.folders.length
                    });
                    this.saveSettings();
                    BdApi.showToast('Added!', { type: 'success' });
                };
                this.createModal({
                    confirmText: 'Save',
                    cancelText: 'Cancel',
                    header: 'New folder',
                    size: ZeresPluginLibrary.Modals.ModalSizes.SMALL,
                    red: false,
                    children: [
                        ZLibrary.DiscordModules.React.createElement(
                            ZLibrary.WebpackModules.getByDisplayName('FormItem'),
                            {
                                className: ZLibrary.WebpackModules.getByProps('input', 'card').input
                            },
                            ZLibrary.DiscordModules.React.createElement(ZLibrary.WebpackModules.getByDisplayName('TextInput'), {
                                ref: e => {
                                    ref = e;
                                },
                                value: folderName,
                                onChange: value => {
                                    ref.props.value = value;
                                    ref.forceUpdate();
                                },
                                autoFocus: 1
                            })
                        )
                    ],
                    onConfirm: () => saveFolder()
                });
            }),
            createItem('Save As...', () => {
                const path = this.openSaveDialog({
                    defaultPath: formattedurl.fileName
                });
                if (!path) return BdApi.showToast('Maybe next time.');
                saveFile(path);
            })
        );
        return createSubMenu(`Save ${type} To`, subItems);
    }
    formatURL(url, requiresSize) {
        if (url.indexOf('/a_') !== -1) url = url.replace('.webp', '.gif');
        else url = url.replace('.webp', '.png');
        const fileName = url.substr(url.lastIndexOf('/') + 1);
        if (requiresSize) url += '?size=2048';
        return {
            fileName: fileName,
            url: url
        };
    }
    handleContextMenu(thisObj, args, returnValue) {
        if (!returnValue) return returnValue;
        const type = thisObj.props.type;

        const createSubmenu = (name, items, callback) => {
            return ZeresPluginLibrary.DiscordModules.React.createElement(this.SubMenuItem, {
                label: name,
                render: items,
                invertChildY: false,
                action: () => {
                    if (callback) {
                        this.ContextMenuActions.closeContextMenu();
                        callback();
                    }
                }
            });
        };

        const createItem = (name, callback) => {
            return ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuItem, {
                label: name,
                action: () => {
                    this.ContextMenuActions.closeContextMenu();
                    if (callback) callback();
                }
            });
        };

        let saveType = 'Image';
        let url = '';

        // image has no type property
        if (!type) {
            if (!thisObj.props.src) return;
            url = thisObj.props.src;
            if (url.indexOf('.mp4') !== -1) saveType = 'Video';
        } else if (type === 'GUILD_ICON_BAR') {
            saveType = 'Icon';
            url = thisObj.props.guild.getIconURL();
            if (!url) return;
        } else {
            saveType = 'Avatar';
            if (!thisObj.props.user || !thisObj.props.user.avatarURL) return ZLibrary.Logger.warn(this.getName(), `Something went wrong, user or avatar URL === undefined, unknown context menu type "${type}" ?`);
            url = thisObj.props.user.avatarURL;
            if (url.startsWith('/assets/')) url = 'https://discordapp.com' + url;
        }

        const submenu = this.constructMenu(url.split('?')[0], saveType, createSubmenu, createItem);
        const group = ZeresPluginLibrary.DiscordModules.React.createElement(this.ContextMenuGroup, {
            children: [submenu]
        });
        let targetGroup;
        if (!type || type === 'GUILD_ICON_BAR') targetGroup = returnValue.props.children;
        else targetGroup = returnValue.props.children.props.children.props.children;

        if (this.settings.contextMenuOnBottom) targetGroup.push(group);
        else targetGroup.unshift(group);
    }
}
